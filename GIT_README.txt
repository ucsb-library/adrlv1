Files that needed to be added after pulling from GIT repo:
The sample configuration has the following ports:
solr/rails: 3009
fedora/jetty: 8987
All paths in this document are relative to the base rails app.
for example: the sample rails app is under /rails/sc_hydra_head

/******************************************************************************************************************************/
Jetty Folder Configuration
/******************************************************************************************************************************/
Copy jetty folder. If the vanilla installation is done then this folder should already be completed.

1. copy sample/jetty/jetty.xml.sample to jetty/etc/jetty.xml
Configure fedora port. Currently the sample file contains port 8987.
Vanilla installation has the default port "8983" configured for test and production environment.

--------------------------------------------------------------------------------------------------------------------------------

2. copy sample/jetty/solrconfig.xml.sample to jetty/solr/development-core/conf/solrconfig.xml
This file contains information for indexing. Search fields indexed needs to be added to this file.
Make sure to update sample file for Solr indexing.

/******************************************************************************************************************************/
Config Folder Configuration Files:
/******************************************************************************************************************************/
Sample ports configured for sample documents are:
blacklight: 3009
fedora: 8987
--------------------------------------------------------------------------------------------------------------------------------
Copy following files in config folder. The configuration for blacklight, fedora and the rails application resides in this folder:
1. boot.rb: Change blacklight port in this file to point to the port configured.
For staging and production, this port is configured to default 3000.
copy sample/config/boot.rb.sample to config/boot.rb

--------------------------------------------------------------------------------------------------------------------------------
2. database.yml
copy sample/config/database.yml.sample to config/database.yml

--------------------------------------------------------------------------------------------------------------------------------
3. environment.rb. Set the environment on the following line:
Change the ENV['RAILS_ENV'] ||= 'development'
copy sample/config/environment.rb.sample to config/environment.rb

--------------------------------------------------------------------------------------------------------------------------------
4. Copy the environments folder. This contains 3 files. Configure email settings based on the current environment in the appropriate file:
development.rb
test.rb
production.rb

copy sample/environments/development.rb.sample to config/environments/development.rb
copy sample/environments/test.rb.sample to config/environments/test.rb
copy sample/environments/production.rb.sample to config/environments/production.rb

For example if development environment needs to be configured. The email settings needs to be changed in development.rb file.
Following are the sample email settings:

#email configurations
config.action_mailer.default_url_options = { host: 'http://10.3.180.1', port: 3009 }
config.action_mailer.delivery_method = :smtp
config.action_mailer.perform_deliveries = true
config.action_mailer.raise_delivery_errors = false
config.action_mailer.default :charset => "utf-8"

config.action_mailer.smtp_settings = {
    :address => "smtp.library.ucsb.edu",
    :port => 25,
    :domain => "library.ucsb.edu",
    :enable_starttls_auto => true
}

--------------------------------------------------------------------------------------------------------------------------------
5. fedora.yml
Configure the fedora port in this file.

copy sample/config/fedora.yml.sample to config/fedora.yml

--------------------------------------------------------------------------------------------------------------------------------
6. ingest.yml
Configure the ezid ID configuration for minting ARK's in this file in the appropriate environment configuration setting.

copy sample/config/ingest.yml.sample to config/ingest.yml

--------------------------------------------------------------------------------------------------------------------------------
7. jetty.yml
Configure appropriate fedora port.

copy sample/config/jetty.yml.sample to config/jetty.yml

--------------------------------------------------------------------------------------------------------------------------------
8. role_map_cucumber.yml, role_map_development.yml, role_map_test.yml, role_map_production.yml
Based on the appropriate environment, configure the emails for admin, staff, community and public users.
For example: For development environment, configure role_map_development.yml file for roles.

copy sample/config/role_map_cucumber.yml.sample to config/role_map_cucumber.yml
copy sample/config/role_map_development.yml.sample to config/role_map_development.yml
copy sample/config/role_map_test.yml.sample to config/role_map_test.yml
copy sample/config/role_map_production.yml.sample to role_map_production/jetty.yml

--------------------------------------------------------------------------------------------------------------------------------
9. solr.yml
Configure fedora port.

copy sample/config/solr.yml.sample to config/solr.yml

--------------------------------------------------------------------------------------------------------------------------------
10. url_config.yml
This file sets different configurations in the application:
rails_url: url for blacklight/rails application.
fed_url: This needs to point to http://localhost:8987/. The port needs to match the fedora port.
rails_port: This port needs to be configured for the rails port.
fedora_url: This is the same url as rails_url without the trailing slash.
fedora_port: This needs to match the fedora port.
ucsb_env: Based on the environment, this would be development, test or production.
from_email: The from address for emails sent out from the application.
to_email: The to address for emails sent out from the application. In development environment, every developer must set it to their own email.
base_url: This need not change until the domain for the rails url changes.

copy sample/config/url_config.yml.sample to config/url_config.yml

/******************************************************************************************************************************/
Nginx Configuration
    /opt/nginx/conf/nginx.conf
/******************************************************************************************************************************/
The following new port needs to be added to the nginx configuration:
    server {
        listen       3009;
        access_log /rails/git_sc_test/log/git_sc_test_nginx.log;
        root /rails/git_sc_test/public;
        passenger_enabled on;
        rails_env development;

        rewrite ^(.*)/lib/ark:/(.*)/(.*)$ $1/catalog/adrl:$3 last;

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }

    }

/******************************************************************************************************************************/
Set permissions on the rails app directory
/******************************************************************************************************************************/
Set read and execute permissions on all directories in your rails application

chmod -R a+rw /rails/git_sc_test/

/******************************************************************************************************************************/
Run db:migrate script
/******************************************************************************************************************************/
Run the db migration scripts
rake db:migrate RAILS_ENV=development

If db migration script fails because of the following error:
rake aborted!
Gem::LoadError: You have already activated rake 10.3.2, but your Gemfile requires rake 10.3.1. Prepending `bundle exec` to your command may solve this.
...

Then rake bundle needs to be updated. Run the following command:
bundle update rake


/******************************************************************************************************************************/

Restart Nginx 
    sudo service nginx restart
Start jetty
    cd jetty/
    rake jetty:start
    