

require 'rest_clientwrapper.rb'


class EzidAdapter

    def initialize(config)
       @config = config
    end

    def mint_ark_from_ezid()

       # initialize config file
       rest = RestClientWrapper.new(@config, 'ezid_url', 'ezid_user', 'ezid_password')
       response = rest.post('ezid_post_uri')

       # extract full ARK from response body
       body = response.body
       body = body.split(' ');
       ark = body[1]
       ark
    end


end


