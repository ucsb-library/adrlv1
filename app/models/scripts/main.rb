require 'marc'
require 'nokogiri'
require 'find'
require 'zip'
require 'csv'

require 'active-fedora'
require './datastreams/mods_article_datastream.rb'
require './datastreams/rights_metadata.rb'
require './etd.rb'
require './process_proquest.rb'
require './process_mods.rb'
require './fedora_service.rb'
require './solr_service.rb'
require './logging.rb'
require './matching_zip_processor.rb'

class Main

     include Logging

     def initialize()
	#create an array of pids
	@main_pids = %w[]
	@main_pids.to_a()
        #create an array of JSON log objects
        @hist_log= %w[]
        @hist_log.to_a()
        @duplicate_rec= %w[]
        @duplicate_rec.to_a()
        @skipped_rec= %w[]
        @skipped_rec.to_a()
	@hash = {}
	CSV.foreach('ark_pdf.csv') do |row|
	  @hash[row[0]] = row[1]
	end
	puts @hash.keys
     end

     def batch_injest(marc_file, zip_path)

    
        xsl = "xslt/MARC21slim2MODS3-4.xsl"

	marcxml_file = "marcxml/marc.xml"	
        #convert MARC to MARCXML record
        MARC::XMLReader.best_available!
	    marc_2_marcxml(marc_file, marcxml_file)

  	# reading a marcxml record
  	reader = MARC::XMLReader.new(marcxml_file)
  	for record in reader
          
          # extract system number from marc record for logging skipped records 
	  record_001 = record["001"]
          system_number = ""
	  if (!record_001.nil? || !record_001.blank?)
            system_number = record_001.value
	  end

          # extract title and author from marc record  
	  record_245 = record["245"]
          title = ""
	  if (!record_245.nil? || !record_245.blank?)
            field_title  = record["245"].find{|subfield| subfield.code =='a'}
            title = field_title.value
	  end

	  #TODO: Read value from external file and read the values and check for null and blank values
	  record_100 = record["100"]
          author = ""
	  if (!record_100.nil? || !record_100.blank?)
            field_author = record["100"].find{|subfield| subfield.code =='a'}
            author = field_author.value
	  end

          # find matching zip and pdf name
          zip_processor = MatchingZipProcessor.new

	  #if (!author || author.to_s.size > 1)
	  #  puts "author in main" + author + "end"
          #end

	  #zip_processor.process_matching_zip(zip_path, title, author)
	  zip_processor.process_matching_zip(zip_path, title, author, system_number)
          pdf_name = ""
          pdf_name = zip_processor.get_attached_content_file
          matching_zip = zip_processor.get_matching_zip_file 

	  if (!pdf_name.nil? || pdf_name.to_s.size > 1)
	    puts "Preparing to ingest record [PDF: " + pdf_name + "]"
	  end

          #matching_zip = zip_processor.get_matching_zip_file 

	  #TODO: Uncomment this code to disable duplicate records insertion
          # revoke ingestion of duplicate objects
          # query solr with pdfname as primary key
	  if (!pdf_name.nil? || pdf_name.to_s.size > 1)
	    @ark_from_csv = @hash.key(pdf_name)

          single_marcxml_file = "marcxml/" + pdf_name + ".xml"
puts "single_marcxml_file: marcxml/" + pdf_name + ".xml: " + single_marcxml_file 

          # puts  single marc record in a xml  
          File.open(single_marcxml_file, 'w') do |f2|
            f2.puts record.to_xml
puts "single marc record: " + record.to_s
          end

#puts "before calling marcxml_2_mods: " + single_marcxml_file

          single_mods_file = "mods/" + pdf_name + ".xml"
          # convert this MARC record to MODS  
          marcxml_2_mods(single_marcxml_file, xsl, single_mods_file)

	  # end loop for checking pdf_name
	  end

          # find matching Proquest Zip  
          #matching_zip = find_matching_zip(zip_path, pdf_name)

	  if matching_zip != nil
           
	   puts "MATCH FOUND [zip: " + matching_zip + ", pdf: " + pdf_name + "]"

           unzip_dir = zip_path + File.basename(matching_zip, ".zip")
           # unzip matching proquest
           unzip_file(zip_path + matching_zip, unzip_dir)

           # process proquest metadata
           proquest_xml = find_proquest_xml(unzip_dir, File.basename(pdf_name, '.pdf'))
           pro = ProcessProquest.new
           pro.proquest_2_mods(proquest_xml, single_mods_file)

           # clean up MODS
           mo = ProcessMods.new
           mo.clean_mods(unzip_dir, single_mods_file)

           # ingest object
           ing = FedoraService.new
	   tmp_pids = ing.ingest(unzip_dir, single_mods_file, File.basename(pdf_name, '.pdf'), @ark_from_csv)

	   @main_pids.push(tmp_pids)

	   hist = ing.get_log()
	   #puts "hist"
	   #puts hist
	   @hist_log.push(hist)

	   #Add script to delete unzip_dir 

	   #skip = zip_processor.get_log()
	   #@skipped_rec.push(skip)

	  # matching zip not found but pdf name is available 
          elsif (!pdf_name.nil? || pdf_name.to_s.size > 1)
           # We should never reach here but is possible
           puts "MATCHING ZIP NOT FOUND [" + pdf_name + "]"

	  # matching zip and pdf name are not available 
          elsif 
	   skip = zip_processor.get_log()
	   @skipped_rec.push(skip)
           # We should never reach here but is possible
           puts "MATCHING ZIP NOT FOUND"
          end 

        end

	puts "final main_pids list"
	puts @main_pids

	puts "final hist_log list"
	puts @hist_log
	
	puts "final skipped_rec list"
	puts @skipped_rec
	
	return @main_pids

     end

     def get_ingest_log()
	return @hist_log
     end

     def get_duplicate_recs()
	return @duplicate_rec
     end

     def get_skipped_recs()
	return @skipped_rec
     end

     def marc_2_marcxml(marc_file, marcxml_file)

       # target MARCXML file to be generated 
       writer = MARC::XMLWriter.new(marcxml_file)

       # reading records from MARC file
#       reader = MARC::Reader.new(File.new(marc_file, "r:binary:binary"),
#                                 :external_encoding => "cp866",
#   			         :internal_encoding => "utf-8")
       reader = MARC::Reader.new(File.new(marc_file, "r:binary:binary"),
                                 :external_encoding => "utf-8",
   			         :internal_encoding => "utf-8")
       for record in reader
         # writing to MARCXML file
         writer.write(record)
puts "marc_2_marcxml: record: " + record.to_s
       end

       # close MARCXML file
       writer.close()
 
    end



    def marcxml_2_mods(src_xml, style, tgt_file)

       doc   = Nokogiri::XML(File.read(src_xml))
	puts "marcxml_2_mods: doc: "
	puts doc
       xslt  = Nokogiri::XSLT(File.read(style))

       # Transform MARCXML to MODS
       response = xslt.transform(doc)
       File.open(tgt_file, 'w') do |f2|
        f2.puts response
       end

	puts "marcxml_2_mods: tgt_file: "
	puts tgt_file

    end

    
    #@depricated
    def find_matching_zip(path, marc_pdf) 
  
     Find.find(path) do |f|

      ext = File.extname(f)
      # Unzip the file
      if ext == '.zip'
        if  is_matching_zip(f, marc_pdf) == "true"
  	 return File.basename(f)
        end
      end
     end

    end

    #@depricated
    def is_matching_zip(file, pdf_name)
     Zip::File.open(file) { |zip_file|
       zip_file.each { |f|
         if File.basename(f.name) == pdf_name
            return "true"
         end
       }
     } 
     return "false"
   end
 


    def unzip_file(file, destination)
     Zip::File.open(file) { |zip_file|
       zip_file.each { |f|
         f_path=File.join(destination, f.name)
	 FileUtils.mkdir_p(File.dirname(f_path))
         zip_file.extract(f, f_path) unless File.exist?(f_path)
       }
     }
    end

    def find_proquest_xml(src_path, pattern)

     Find.find(src_path) do |f|

      ext = File.extname(f)
      if File.fnmatch(pattern + '*.xml', File.basename(f)) 
         return File.path(f)
      end
     end
    end


end




# MAIN

#puts "MAIN"

main = Main.new
main.batch_injest("/rails/sc_hydra_head/public/data/marc/batch_enc.mrc", "/rails/sc_hydra_head/public/data/etd/batch_enc/")
main.batch_injest("/rails/sc_hydra_head/public/data/marc/ETDSample_Batch3.mrc", "/rails/sc_hydra_head/public/data/etd/batch_3/")

#marc.batch_injest("marcxml/ETDsampleExportMARCXML-ver2.xml", "etd/")
#marc.batch_injest("marc/ETDsampleExport_rev2.mrc", "etd/")

# END

