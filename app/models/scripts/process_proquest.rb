

require 'nokogiri'

require 'active-fedora'
require './datastreams/mods_article_datastream.rb'

#
# This class does the proquest xml processing of an ETD record and
# adds new elements to the mods

class ProcessProquest 

    # Depricated: not used now
    def process_xml(path) 
    
 
     # PULL DATA       
     xml = Nokogiri::XML(File.open("Winkler_ucsb_0035_11066_DATA.xml")) 
     xml.xpath("//DISS_advisor/DISS_name").each do |node|
      puts ">>>>>>>"
      puts node.xpath("DISS_surname").text
      puts node.xpath("DISS_fname").text
      puts node.xpath("DISS_middle").text       
     end

     xml.xpath("//DISS_cmte_member/DISS_name").each do |node|
      puts ">>>>>>>11"
      puts node.xpath("DISS_surname").text
      puts node.xpath("DISS_fname").text
      puts node.xpath("DISS_middle").text
     end

     xml.xpath("//DISS_categorization").each do |node|
      puts ">>>>>>>"
      puts node.xpath("DISS_keyword").text
     end 

     xml.xpath("//DISS_categorization/DISS_category").each do |node|
      puts ">>>>>>>"
      puts node.xpath("DISS_cat_desc").text
     end
    
     xml.xpath("//DISS_description").each do |node|
      puts ">>>>>>>"
      puts node.xpath("DISS_degree").text
     end

 
     puts xml.xpath("//DISS_description/@type").text
     puts ">>>>>>>>>"
         
     # PUSH Data	

     @mods = File.open("mods/Winkler_ucsb_0035_11066.pdf.xml")
     @ds = ModsArticleDatastream.from_xml(@mods) 

     @ds.extension.degree.name = "Masters"
     node =  @ds.insert_contributor(:person)

     
     namePart = node[0].css("namePart")
     namePart[0].content = "Banerjee"
     namePart[1].content = "Kaustav J."

     roleTerm = node[0].at_css("roleTerm")
     roleTerm.content = "Thesis Advisor"	

      #adding addribute
     roleTerm["authority"] = "marcrelator" 


     puts "Result: " + node[0].to_xml
    


  end 

   
  # public interface method which takes proquest xml and 
  # mods xml as inputs and processes them 
  def proquest_2_mods(proquest_file, mods_file)
 
     # source file   
     proquest_xml = Nokogiri::XML(File.open(proquest_file))
     
     # target file
     @mods = File.open(mods_file)
     @ds = ModsArticleDatastream.from_xml(@mods)

     # add advisor
     add_person("DISS_advisor", proquest_xml)
     
     # add committe member
     add_person("DISS_cmte_member", proquest_xml)

     # add subject
     add_subject(proquest_xml)

     # adding etd namesapce to MODS before adding acutal node 
     # this process uses metadata standard as shown
     @ds.insert_namespace('etd', 'http://www.ndltd.org/standards/metadata/etdms/1.0')

     # add extension
     add_extension(proquest_xml)

     # Format final MODS xml
     doc = Nokogiri::XML(@ds.to_xml) { |x| x.noblanks }
   
     # Rename old mods xml  
     File.rename(mods_file, mods_file + "_PROCESSED")
 
     # create new mods xml with proquest info
     File.open(mods_file, 'w') do |f2|  
     f2.puts doc
     end  
    
     puts "DONE"
 
  end
  
  # adds and populates new person elements to the mods xml

  def add_person(type, proquest_xml)

     proquest_xml.xpath("//" + type + "//DISS_name").each do |node|
     
      person_node =  @ds.insert_contributor(:person)      
 
      name_part = person_node[0].css("namePart")
      name_part[0].content = node.xpath("DISS_surname").text
      
      #process middle name
      middle_name = node.xpath("DISS_middle").text
      if middle_name

        if middle_name.bytesize == 1 
          middle_name = " " + middle_name + "."           
        elsif middle_name.bytesize > 1
          middle_name = " "  + middle_name
        end        

      end
      name_part[1].content = node.xpath("DISS_fname").text + middle_name

     role_term = person_node[0].at_css("roleTerm")
     
     if type == "DISS_advisor"
      role_term.content = "Thesis advisor"
     elsif type == "DISS_cmte_member"
      role_term.content = "Committee member"
     end

     #adding attribute
     role_term["authority"] = "marcrelator"
     
   end

 
  end


  # adds and populates new subject element to the mods xml
  def add_subject(proquest_xml)


     keywords = proquest_xml.xpath("//DISS_categorization/DISS_keyword").text
     key_Arr = keywords.split(/,/) 
     
     # create new subject with topic for  each keyword   
     key_Arr.each do |element|	

     subject_node =  @ds.insert_subject(:subject)
     topic = subject_node[0].css("topic") 
     element.strip!
     topic[0].content = element
     end

  end


  # adds and populates new extension element to the mods xml
  def add_extension(proquest_xml)

     ext_node =  @ds.insert_extension(:extension)

     # Adding discipline dynamically as we don't know the
     # number of discipline nodes required before head
     degree = ext_node[0].at_css("degree")
     proquest_xml.xpath("//DISS_categorization/DISS_category").each do |node|
      discipline = Nokogiri::XML::Node.new "etd:discipline", degree
      discipline.content = node.xpath("DISS_cat_desc").text
      degree.add_child(discipline)
     end
     # adding namepace to the degree node
     degree.name = "etd:degree"

     name = ext_node[0].at_css("name")
     name.content = proquest_xml.xpath("//DISS_description/DISS_degree").text
     name.name = "etd:name"

     level = ext_node[0].at_css("level")
     type = proquest_xml.xpath("//DISS_description/@type").text
     level.content = type.capitalize if type
     level.name = "etd:level"

  end



end




#MAIN

puts " Processing PROQUEST"

#pro = ProcessProquest.new

#pro.process_xml("")
#pro.proquest_2_mods("Winkler_ucsb_0035_11066_DATA.xml","mods/Winkler_ucsb_0035_11066.pdf.xml")


#END
