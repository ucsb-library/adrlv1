
require 'nokogiri'
require 'date'

#
# This class takes care of RightsMetadata
# and embargo code related processing  
#
class ProcessRightsMetadata

     
     # A public interface method which takes care of
     # Embargo code processing
     def process_rights_metadata(proquest_file, rightsMeta_xml) 
 
       #puts 'Processing Embargo code...'
    
       xml = Nokogiri::XML(File.open(proquest_file))
       embargo = xml.xpath('//DISS_submission/@embargo_code')
     
       if embargo
         
         embargo_code = embargo.text
         if embargo_code == '0'
           #puts 'Embargo code is 0, no need to process' 
           return   
         elsif embargo_code == '1' || embargo_code == '2' || embargo_code == '3'
           accept_date = xml.xpath('//DISS_description/DISS_dates/DISS_accept_date')
           release_date = compute_rel_date(embargo_code, accept_date)
         else
      	   remove_date = xml.xpath('//DISS_sales_restriction/@remove')
           release_date = compute_rel_date_special(remove_date)
         end 

       end

       #puts 'Processed release date: ' + release_date.to_s

       builder = Nokogiri::XML::Builder.new do |xml|
         xml.date(:type=>"release")
       end
    
       date = builder.doc.root
       date.content = release_date     

       rightsMeta_xml = Nokogiri::XML(rightsMeta_xml)  
       human  = rightsMeta_xml.at_css('//embargo/human')
       human.content = (!release_date && embargo_code == '4')? 'infinite embargo' : release_date
 
       machine  = rightsMeta_xml.at_css('//embargo/machine')
       machine.add_child(date) 

       formatted_xml = Nokogiri::XML(rightsMeta_xml.to_xml) { |x| x.noblanks }     
       return formatted_xml
     end
     
     # computes relate based when embargo cade is between
     # range 1..3 
     def compute_rel_date(embargo_code, accept_date)
   
       case embargo_code
         when '0'
           months = 0
         when '1'
           months = 6
         when '2'
           months = 12
         when '3'
           months = 24
         else
           months = 0
       end

       if accept_date
        #puts 'Unprocessed Date: ' + accept_date.text
        d = Date.parse(accept_date.text)

        # get date in YYYY-12-31 format
        d = d >> 11
        d = d + 30
        
        # add months as per embargo code
        d = d >> months

       return d    
      end

     end 

     # computes release date when embargo
     # code is 4, special case 
     def compute_rel_date_special(remove_date)

       if remove_date
          if !remove_date.text.empty?
           #puts 'Extracted remove date: ' + remove_date.text 
           return Date.strptime(remove_date.text, '%m/%d/%Y')           
          end  
       end

     end 

     # adds copyrights information to rights metadata 
     def add_copyright(mods_file, rightsMeta_xml)

       #puts 'Adding Copyright info to RightsMetadata...'

       xml = Nokogiri::XML(File.open(mods_file))
       fm_name_node = xml.xpath('.//mods:name[@usage="primary"]/mods:namePart[@type="given"]',
                                {'mods' => 'http://www.loc.gov/mods/v3'})
       l_name_node = xml.xpath('.//mods:name[@usage="primary"]/mods:namePart[@type="family"]',
                               {'mods' => 'http://www.loc.gov/mods/v3'})

       fm_name = fm_name_node.text if fm_name_node
       l_name = l_name_node.text if l_name_node

       date_issued_node = xml.xpath('.//mods:originInfo/mods:dateIssued[@encoding="iso8601"]',
                                    {'mods' => 'http://www.loc.gov/mods/v3'})

       date_issued = date_issued_node.text if date_issued_node

       rmd_xml = Nokogiri::XML(rightsMeta_xml)
       copyright_title  = rmd_xml.at_css('//copyright/human[@type="title"]')
       copyright_desc  = rmd_xml.at_css('//copyright/human[@type="description"]')
       copyright_title.content = 'Copyright'
       #copyright_desc.content = '&#169;' + ' Copyright ' + date_issued.to_s + ' by ' + fm_name.to_s + ' ' + l_name.to_s
       copyright_desc.content = "\xC2\xA9" + ' Copyright ' + date_issued.to_s + ' by ' + fm_name.to_s + ' ' + l_name.to_s

       return rmd_xml

     end

     # adds additional access information to rights metadata 
     def add_additional_access(rightsMeta_xml)

       #puts 'Adding additional access info to RightsMetadata...'
       rmd_xml = Nokogiri::XML(rightsMeta_xml)
       
       # create access node
       builder = Nokogiri::XML::Builder.new do |xml|
	 xml.access(:type=>"delete"){
	   xml.human
	   xml.machine{
	     xml.group("admin")
	   }
	 }
       end
	 
       node = builder.doc.root
       #puts "node"
       #puts node

       # get ref of last access node in mods
       access = rmd_xml.css('//access').last
       #puts access
       if access
         access.after(node)
       else
         rmd_xml.root.last_element_child.add_child(node)
       end
       
       return rmd_xml

     end

end


#MAIN

#p  = ProcessRightsMetadata.new
#p.process_embargo('Shishim_ucsb_0035D_11180_DATA.xml', '')
