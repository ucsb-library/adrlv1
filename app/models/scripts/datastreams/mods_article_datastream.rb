
require "active-fedora"

class ModsArticleDatastream < ActiveFedora::OmDatastream

    set_terminology do |t|
      t.root(:version=>"3.4", :path=>"mods",
             :xmlns=>"http://www.loc.gov/mods/v3", 
	     "xmlns:etd"=>"http://www.ndltd.org/standards/metadata/etdms/1.0",             
             :schema=>"http://www.loc.gov/standards/mods/v3/mods-3-4.xsd")
t.etd(:path=>"etd", "xmlns:etd"=>"http://www.ndltd.org/standards/metadata/etdms/1.0")

      t.title_info(:path=>"titleInfo", :attributes=>{:usage=>"primary"}) {
        t.non_sort(:path=>"nonSort")
        t.main_title(:path=>"title", :label=>"title")
        t.sub_title(:path=>"subTitle")
        t.language(:path=>{:attribute=>"lang"})
      }

      t.title_info_alt(:path=>"titleInfo", :attributes=>{:usage=>"alternative"}) {
        t.non_sort(:path=>"nonSort")
        t.main_title(:path=>"title", :label=>"title")
        t.sub_title(:path=>"subTitle")
        t.language(:path=>{:attribute=>"lang"})
      }

      t.language{
        t.lang_code(:index_as=>[:facetable, :stored_searchable], :path=>"languageTerm", :attributes=>{:type=>"code"})
      }
      t.abstract(:index_as=>[:stored_searchable])
      t.subject {
        t.topic(:index_as=>[:facetable, :stored_searchable])
      }
      t.topic_tag(:proxy=>[:subject, :topic])
      # t.topic_tag(:index_as=>[:facetable],:path=>"subject", :default_content_path=>"topic")
      # This is a mods:name. The underscore is purely to avoid namespace conflicts.
      t.name_(:index_as=>[:searchable, :stored_searchable, :facetable]) {
        # this is a namepart
        t.namePart(:type=>:string, :label=>"generic name", :index_as=>[:stored_searchable, :facetable])
        # affiliations are great
        t.affiliation
        t.institution(:path=>"affiliation", :index_as=>[:facetable], :label=>"organization")
        t.displayForm
        t.role(:ref=>[:role])
        t.description(:index_as=>[:facetable])
        t.date(:path=>"namePart", :attributes=>{:type=>"date"}, :index_as=>[:facetable, :stored_searchable])
        t.last_name(:path=>"namePart", :attributes=>{:type=>"family"})
        t.first_name(:path=>"namePart", :attributes=>{:type=>"given"}, :label=>"first name")
        t.terms_of_address(:path=>"namePart", :attributes=>{:type=>"termsOfAddress"})
        t.computing_id
      }

      t.names(:path => "name", :index_as=>[:searchable, :stored_searchable, :facetable]){
	t.namePart(:type=>:string, :label=>"generic name", :index_as=>[:stored_searchable, :facetable])
	t.family_name(:path=>"namePart", :attributes=>{:type=>"family"}, :index_as=>[:stored_searchable, :facetable])
	t.given_name(:path=>"namePart", :attributes=>{:type=>"given"}, :index_as=>[:stored_searchable, :facetable])
      }

      # lookup :person, :first_name
      t.person(:ref=>:name, :attributes=>{:type=>"personal"}, :index_as=>[:facetable, :stored_searchable])
      t.department(:proxy=>[:person,:description],:index_as=>[:facetable])
      t.organization(:ref=>:name, :attributes=>{:type=>"corporate"}, :index_as=>[:facetable, :stored_searchable])
      t.conference(:ref=>:name, :attributes=>{:type=>"conference"}, :index_as=>[:facetable])
      t.role {
        t.text(:path=>"roleTerm",:attributes=>{:type=>"text"}, :index_as=>[:stored_searchable])
        t.code(:path=>"roleTerm",:attributes=>{:type=>"code"})
      }

      #For indexing
      t.personal_author(:path=>"name", :attributes=>{:type=>"personal"}, :index_as=>[:stored_searchable, :facetable]) {
        t.part(:path=>"namePart",:index_as=>[:facetable])
      }

      t.committee_member(:path=>"name", :attributes=>{:type=>"personal"}, :index_as=>[:stored_searchable, :facetable]) {
        t.part(:path=>"namePart",:index_as=>[:facetable])
	t.role{
	  #t.text(:path=>"roleTerm",:attributes=>{:type=>"text"}, :index_as=>[:stored_searchable], :value="Committee Member")
	  t.text(:path=>"roleTerm",:attributes=>{:type=>"text"}, :index_as=>[:stored_searchable])
	}
      }

      #t.advisor(:path => 'name/role/roleTerm/["Thesis advisor"]', :index_as=>[:stored_searchable, :facetable])

      t.origin_info(:path=>"originInfo", :index_as=>[:stored_searchable]) {
          t.place(:ref=>[:place]) 
          t.publisher(:index_as=>[:stored_searchable])
          t.date_issued(:path=>"dateIssued", :index_as=>[:facetable])
          t.date_issued_enc(:path=>"dateIssued", :attributes=>{:encoding=>"marc"}, :index_as=>[:stored_searchable])
          t.date_created_key(:path=>"dateCreated", :attributes=>{:encoding=>"iso8601", :keyDate=>"yes"}, :index_as=>[:facetable, :stored_searchable])
          t.issuance(:index_as=>[:facetable])
         
      }

      #t.dates(:path => 'originInfo/@encoding="iso8601"', :index_as=>[:facetable, :stored_searchable])
      #t.dates(:path => 'originInfo/:attributes=>{encoding="iso8601"}', :index_as=>[:facetable, :stored_searchable])
      #t.dates(:path => 'originInfo/date*[encoding="iso8601"]', :index_as=>[:facetable, :stored_searchable])
#      t.dates(:path => 'originInfo/dateCreated', :index_as=>[:facetable, :stored_searchable])

#      t.ext_degree_discipline(:path => 'extension/etd:degree/etd:discipline', :xmlns=> "http://www.ndltd.org/standards/metadata/etdms/1.0", :index_as=>[:facetable, :stored_searchable])



      t.origin_info_date(:path=>"originInfo", :index_as=>[:facetable]) {
	t.date_issued(:path=>"dateIssued", :index_as=>[:facetable])
	t.date_created_key(:path=>"dateCreated", :attributes=>{:encoding=>"iso8601", :keyDate=>"yes"}, :index_as=>[:facetable, :stored_searchable])
      }

      t.place {
        t.text(:path=>"placeTerm",:attributes=>{:type=>"text"}, :index_as=>[:facetable, :stored_searchable])
        t.code(:path=>"placeTerm",:attributes=>{:type=>"code"})
      }

      t.journal(:path=>'relatedItem', :attributes=>{:type=>"host"}) {
        t.title_info(:index_as=>[:facetable],:ref=>[:title_info])

        t.origin_info(:ref=>[:origin_info]) 

        t.issue(:path=>"part") {
          t.volume(:path=>"detail", :attributes=>{:type=>"volume"}, :default_content_path=>"number")
          t.level(:path=>"detail", :attributes=>{:type=>"number"}, :default_content_path=>"number")
          t.extent
          t.pages(:path=>"extent", :attributes=>{:unit=>"pages"}) {
            t.start
            t.end
          }
          t.start_page(:proxy=>[:pages, :start])
          t.end_page(:proxy=>[:pages, :end])
          t.publication_date(:path=>"date", :type=>:date, :index_as=>[:stored_searchable])
        }
      }
      t.note

      t.note_sor(:path=>"note",  :attributes=>{:type=>"statement of responsibility"}, :index_as=>[:stored_searchable])
      #note with no attribute
      t.note_advisor(:path=>"note", :index_as=>[:stored_searchable])
      t.note_thesis(:path=>"note",  :attributes=>{:type=>"thesis"}, :index_as=>[:stored_searchable])
      t.note_bibliography(:path=>"note", :attributes=>{:type=>"bibliography"}, :index_as=>[:stored_searchable])

      t.location(:path=>"location") {
        t.url(:path=>"url")
      }
      t.publication_url(:proxy=>[:location,:url])
      t.peer_reviewed(:proxy=>[:journal,:origin_info,:issuance], :index_as=>[:facetable])
      t.title(:proxy=>[:title_info], :index_as=>[:facetable, :stored_searchable])
      t.journal_title(:proxy=>[:journal, :title_info, :main_title])
     
      #For indexing
      t.author(:path=>"name", :attributes=>{:type=>"personal"}, :attributes=>{:usage=>"primary"}) {
        t.part(:path=>"namePart",:index_as=>[:stored_searchable, :facetable])
      }

      t.oclc(:path =>"identifier", :attributes=>{:type=>"oclc"}, :index_as=>[:stored_searchable]) 
      t.isbn(:path =>"identifier", :attributes=>{:type=>"isbn"}, :index_as=>[:stored_searchable])
      t.aleph_sys_number(:path =>"identifier", :attributes=>{:type=>"system"}, :index_as=>[:stored_searchable])     
 
      t.record_info(:path=>"recordInfo") {
        t.description_standard(:path=>"descriptionStandard")
        t.record_content_source(:path=>"recordContentSource", :attributes=>{:authority=>"marcorg"})
        t.record_creation_date(:path=>"recordCreationDate", :attributes=>{:encoding=>"iso8601"})
        #t.record_creation_date(:path=>"recordCreationDate", :attributes=>{:encoding=>"iso8601"})
        t.record_change_date(:path=>"recordChangeDate", :attributes=>{:encoding=>"iso8601"})
        #t.aleph_number(:path=>"recordIdentifier", :attributes=>{:source=>"OCoLC"},  :index_as=>[:stored_searchable]) 
        #t.aleph_number(:path=>"recordIdentifier", :index_as=>[:stored_searchable]) 
        #t.record_identifier(:path=>"recordIdentifier")
        t.record_origin(:path=>"recordOrigin")
	t.lang_cat(:path=>"languageOfCataloging", :attributes=>{:usage=>"primary"}){
          t.lang_code(:path=>"languageTerm", :attributes=>{:type=>"code"})
	}
      }   
  
      # new fields 
      t.extension(:path=>"extension", :index_as=>[:stored_searchable]){
        t.degree(:path=>"degree", :index_as=>[:stored_searchable], :xmlns => "http://www.ndltd.org/standards/metadata/etdms/1.0") {
          t.name(:path=>"name", :index_as=>[:stored_searchable], :xmlns => "http://www.ndltd.org/standards/metadata/etdms/1.0")
          t.level(:path=>"level", :index_as=>[:stored_searchable], :xmlns => "http://www.ndltd.org/standards/metadata/etdms/1.0")
          t.discipline(:path=>"discipline", :index_as=>[:stored_searchable], :xmlns => "http://www.ndltd.org/standards/metadata/etdms/1.0")
        }
      }

        t.degree(:path=>"degree", :index_as=>[:stored_searchable]) {
          t.name(:path=>"name", :index_as=>[:stored_searchable])
          t.level(:path=>"level", :index_as=>[:stored_searchable])
          t.discipline(:path=>"discipline", :index_as=>[:stored_searchable])
        }

      #t.extension(:path=>"extension"){
      #  t.degree(:path=>{:etd => "degree"}) {
      #    t.name(:path=>"name")
      #    t.level(:path=>"level")
      #    t.discipline(:path=>"discipline")
      #  }
      #}

      #New MODS fields added for update part of CRUD
      #Physical description
      t.physical_desc(:path=>"physicalDescription"){
	t.marcform(:path=>"form", :attributes=>{:authority=>"marcform"}, :index_as=>[:stored_searchable])
	t.gmd(:path=>"form", :attributes=>{:authority=>"gmd"}, :index_as=>[:stored_searchable])
	t.marccategory(:path=>"form", :attributes=>{:authority=>"marccategory"}, :index_as=>[:stored_searchable])
	t.marcsmd(:path=>"form", :attributes=>{:authority=>"marcsmd"}, :index_as=>[:stored_searchable])
	t.media(:path=>"internetMediaType")
	t.extent(:path=>"extent", :index_as=>[:stored_searchable])
	t.digital_origin(:path=>"digitalOrigin")
      }

      #Physical Location
      t.physical_location(:path=>"location") {
        t.physical_loc(:path=>"physicalLocation")
        t.holding_simple(:path=>"holdingSimple"){
	  t.copy_information(:path=>"copyInformation"){
	    t.sub_location(:path=>"subLocation")
	    t.shelf_locator(:path=>"shelfLocator")
	  }
	}
      }

      #Proquest URL
      t.proquest_location(:path=>"location") {
        t.url(:path=>"url", :attributes=>{:note=>"ProQuest full text. Restricted to UC campuses"})
      }

      #ADRL URL
      t.adrl_location(:path=>"location") {
        t.url(:path=>"url", :attributes=>{:usage=>"primary"}, :index_as=>[:stored_searchable])
        t.url_preview(:path=>"url", :attributes=>{:access=>"preview"})
      }

      #Type of Resource
      t.resource_type(:path=>"typeOfResource")

      t.genre(:path=>"genre", :index_as=>[:stored_searchable, :facetable])	
      #genre => marcgt
      t.genre_marcgt(:path=>"genre", :attributes=>{:authority=>"marcgt"}, :index_as=>[:stored_searchable, :facetable])

      #genre => local
      t.genre_local(:path=>"genre", :attributes=>{:authority=>"local"}, :index_as=>[:stored_searchable, :facetable])

      #Fields added for update part of CRUD
      #title
      t.title_nonSort(:proxy=>[:title_info, :non_sort])
      t.title_main_title(:proxy=>[:title_info, :main_title])
      t.title_sub_title(:proxy=>[:title_info, :sub_title])

      t.author_full_name(:path=>"name", :attributes=>{:type=>"personal"}, :attributes=>{:usage=>"primary"}) {
        t.family(:path=>"namePart", :attributes=>{:type=>"family"})
        t.given(:path=>"namePart", :attributes=>{:type=>"given"})
        t.author_role(:path=>"role"){
          t.text(:path=>"roleTerm",:attributes=>{:type=>"text"})
        }
      }

      #For Indexing
      t.personal_full_name(:path=>"name", :attributes=>{:type=>"personal"}, :index_as=>[:facetable]) {
        t.family(:path=>"namePart", :attributes=>{:type=>"family"}, :index_as=>[:facetable])
        t.given(:path=>"namePart", :attributes=>{:type=>"given"}, :index_as=>[:facetable])
      }


      #t.names(:proxy=>[:person, :namePart])
      #t.author(ref: [:person, :namePart])
      #t.author_name(:proxy => [:author, :part])
      t.author_family(:proxy => [:author_full_name, :family])
      t.author_given(:proxy => [:author_full_name, :given])
      t.a_role(:proxy => [:author_full_name, :author_role, :text])

      t.subjects(:proxy=>[:subject, :topic])

      #personal_names
      t.pers_first_names(:proxy => [:person, :first_name])
      t.pers_last_names(:proxy => [:person, :last_name])
      t.pers_roles_text(:proxy => [:person, :role, :text])

      #For indexing
      t.personal_names(:proxy => [:person, :namePart], :index_as=>[:facetable]) 
	
      #corporate names
      t.corporate_names(:proxy => [:organization, :namePart], :index_as=>[:stored_searchable, :facetable])
      t.c_role(:proxy => [:organization, :role, :text])

      #Type of Resource
      t.res_type(:proxy => [:resource_type])

      #genre => marcgt
      t.g_marcgt(:proxy => [:genre_marcgt])

      #genre => local
      t.g_local(:proxy => [:genre_local])

      #origin info
      t.place_code(:proxy => [:place, :code])
      t.place_text(:proxy => [:place, :text])
      t.publisher_name(:proxy => [:origin_info, :publisher])
      t.create_key_date(:proxy => [:origin_info, :date_created_key])
      t.year(:proxy => [:origin_info, :date_issued])
      #t.year_enc(:proxy => [:origin_info, :date_issued])
      t.origin_issuance(:proxy => [:origin_info, :issuance])

      t.lang_name(:proxy => [:language, :lang_code])

      #Physical description
      t.marcform(:proxy => [:physical_desc, :marcform])
      t.gmd(:proxy => [:physical_desc, :gmd])
      t.marccategory(:proxy => [:physical_desc, :marccategory])
      t.marcsmd(:proxy => [:physical_desc, :marcsmd])
      t.media(:proxy => [:physical_desc, :media])
      t.extent(:proxy => [:physical_desc, :extent])
      t.digital_origin(:proxy => [:physical_desc, :digital_origin])

      t.abstract_para(:proxy => [:abstract])

      #note
      t.notes(:proxy => [:note])

      #Physical Location
      t.phy_loc(:proxy => [:physical_location, :physical_loc])
      t.sub_loc(:proxy => [:physical_location, :holding_simple, :copy_information, :sub_location])
      t.shelf_loc(:proxy => [:physical_location, :holding_simple, :copy_information, :shelf_locator])

      #Proquest URL
      t.proquest_loc(:proxy => [:proquest_location, :url])

      #ADRL URL
      t.adrl_loc(:proxy => [:adrl_location, :url])
      t.adrl_preview(:proxy => [:adrl_location, :url_preview])


      #record_info
      t.desc_std(:proxy => [:record_info, :description_standard])
      t.record_content_src(:proxy => [:record_info, :record_content_source])
      t.record_date_create(:proxy => [:record_info, :record_creation_date])
      t.record_date_change(:proxy => [:record_info, :record_change_date])
      #t.record_ocolc(:proxy => [:record_info, :aleph_number])
      #t.record_id(:proxy => [:record_info, :record_identifier])
      t.rec_origin(:proxy => [:record_info, :record_origin])
      t.rec_lang(:proxy => [:record_info, :lang_cat, :lang_code])

      #extension	
      #t.ext_degree_name(:proxy => [:extension, :degree, :name])
      #t.ext_degree_level(:proxy => [:extension, :degree, :level])
      #t.ext_degree_disc(:proxy => [:extension, :degree, :discipline])
      t.ext_degree_name(:path => 'extension/etd:degree/etd:name', :xmlns=> "http://www.ndltd.org/standards/metadata/etdms/1.0", :index_as=>[:facetable, :stored_searchable])
      t.ext_degree_level(:path => 'extension/etd:degree/etd:level', :xmlns=> "http://www.ndltd.org/standards/metadata/etdms/1.0", :index_as=>[:facetable, :stored_searchable])
      t.ext_degree_discipline(:path => 'extension/etd:degree/etd:discipline', :xmlns=> "http://www.ndltd.org/standards/metadata/etdms/1.0", :index_as=>[:facetable, :stored_searchable])

    end

    # Generates an empty Mods Article (used when you call ModsArticle.new without passing in existing xml)
    def self.xml_template
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.mods(:version=>"3.4", "xmlns:xlink"=>"http://www.w3.org/1999/xlink",
           "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance",
           "xmlns"=>"http://www.loc.gov/mods/v3",
	   "xmlns:etd"=>"http://www.ndltd.org/standards/metadata/etdms/1.0",
           "xsi:schemaLocation"=>"http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-4.xsd") {
             xml.titleInfo(:lang=>"") {
               xml.title
             }
             xml.name(:type=>"personal") {
               xml.namePart(:type=>"given")
               xml.namePart(:type=>"family")
               xml.affiliation
               xml.computing_id
               xml.description
               xml.role {
                 xml.roleTerm("Author", :authority=>"marcrelator", :type=>"text")
               }
             }
             xml.typeOfResource
             xml.genre(:authority=>"marcgt")
             xml.language {
               xml.languageTerm(:authority=>"iso639-2b", :type=>"code")
             }
             xml.abstract
             xml.subject {
               xml.topic
             }
             xml.relatedItem(:type=>"host") {
               xml.titleInfo {
                 xml.title
               }
               xml.identifier(:type=>"issn")
               xml.originInfo {
                 xml.publisher
                 xml.dateIssued
                 xml.issuance
               }
               xml.part {
                 xml.detail(:type=>"volume") {
                   xml.number
                 }
                 xml.detail(:type=>"number") {
                   xml.number
                 }
                 xml.extent(:unit=>"pages") {
                   xml.start
                   xml.end
                 }
                 xml.date
               }
             }
             xml.location {
               xml.url
             }
        }
      end
      return builder.doc
    end
    
    # Generates a new Person node
    def self.person_template
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.name(:type=>"personal") {
          xml.namePart(:type=>"family")
          xml.namePart(:type=>"given")
          xml.role {
            xml.roleTerm(:type=>"text")
          }
        }
      end
      return builder.doc.root
    end

    def self.full_name_template
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.full_name(:type => "personal")
      end
      return builder.doc.root
    end

    # Generates a new Organization node
    # Uses mods:name[@type="corporate"]
    def self.organization_template
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.name(:type=>"corporate") {
          xml.namePart
          xml.role {
            xml.roleTerm(:authority=>"marcrelator", :type=>"text")
          }
        }
      end
      return builder.doc.root
    end
    
    # Generates a new Conference node
    def self.conference_template
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.name(:type=>"conference") {
          xml.namePart
          xml.role {
            xml.roleTerm(:authority=>"marcrelator", :type=>"text")
          }
        }
     end
      return builder.doc.root
    end
   
    # Generate new subject
    def self.subject_template
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.subject {
          xml.topic
        }
      end
      return builder.doc.root
    end

    # Generate new extension
    def self.extension_template
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.extension {
             xml.degree {
               xml.name
               xml.level
             } 
       }
      end
      return builder.doc.root
    end

    # Inserts a new contributor (mods:name) into the mods document
    # creates contributors of type :person, :organization, or :conference
    def insert_contributor(type, opts={})
      
     case type.to_sym
      when :person
        node = ModsArticleDatastream.person_template
        nodeset = self.find_by_terms(:person)
      when :organization
        node = ModsArticleDatastream.organization_template
        nodeset = self.find_by_terms(:organization)
      when :conference
        node = ModsArticleDatastream.conference_template
        nodeset = self.find_by_terms(:conference)
      else
        ActiveFedora.logger.warn("#{type} is not a valid argument for ModsArticleDatastream.insert_contributor")
        node = nil
        index = nil
      end
      
      unless nodeset.nil?
        if nodeset.empty?
          self.ng_xml.root.add_child(node)
          index = 0
        else
          nodeset.after(node)
          index = nodeset.length
        end
        #self.dirty = true
      end
      
      return node, index
    end
  
    # Insert new subject 
    def insert_subject(type, opts={})

       node = ModsArticleDatastream.subject_template
       nodeset = self.find_by_terms(type)
         
       unless nodeset.nil?
        if nodeset.empty?
         self.ng_xml.root.add_child(node)
         index = 0
        else
         nodeset.after(node)
         index = nodeset.length
        end
       end
        
       return node, index
    end

    # Insert new extension
    def insert_extension(type, opts={})

       node = ModsArticleDatastream.extension_template
       nodeset = self.find_by_terms(type)

       unless nodeset.nil?
        if nodeset.empty?
         self.ng_xml.root.add_child(node)
         index = 0
        else
         nodeset.after(node)
         index = nodeset.length
        end
       end

       return node, index
    end

    def insert_namespace(prefix, namespace)
       self.ng_xml.root.add_namespace(prefix, namespace)
    end
 
    # Remove the contributor entry identified by @contributor_type and @index
    def remove_contributor(contributor_type, index)
      self.find_by_terms( {contributor_type.to_sym => index.to_i} ).first.remove
      self.dirty = true
    end
    
    def self.common_relator_terms
       {"aut" => "Author",
        "clb" => "Collaborator",
        "com" => "Compiler",
        "ctb" => "Contributor",
        "cre" => "Creator",
        "edt" => "Editor",
        "ill" => "Illustrator",
        "oth" => "Other",
        "trl" => "Translator",
        }
    end
    
    def self.person_relator_terms
      {"aut" => "Author",
       "clb" => "Collaborator",
       "com" => "Compiler",
       "cre" => "Creator",
       "ctb" => "Contributor",
       "edt" => "Editor",
       "ill" => "Illustrator",
       "res" => "Researcher",
       "rth" => "Research team head",
       "rtm" => "Research team member",
       "trl" => "Translator"
       }
    end
    
    def self.conference_relator_terms
      {
        "hst" => "Host"
      }
    end
    
    def self.organization_relator_terms
      {
        "fnd" => "Funder",
        "hst" => "Host"
      }
    end
    
    def self.dc_relator_terms
       {"acp" => "Art copyist",
        "act" => "Actor",
        "adp" => "Adapter",
        "aft" => "Author of afterword, colophon, etc.",
        "anl" => "Analyst",
        "anm" => "Animator",
        "ann" => "Annotator",
        "ant" => "Bibliographic antecedent",
        "app" => "Applicant",
        "aqt" => "Author in quotations or text abstracts",
        "arc" => "Architect",
        "ard" => "Artistic director ",
        "arr" => "Arranger",
        "art" => "Artist",
        "asg" => "Assignee",
        "asn" => "Associated name",
        "att" => "Attributed name",
        "auc" => "Auctioneer",
        "aud" => "Author of dialog",
        "aui" => "Author of introduction",
        "aus" => "Author of screenplay",
        "aut" => "Author",
        "bdd" => "Binding designer",
        "bjd" => "Bookjacket designer",
        "bkd" => "Book designer",
        "bkp" => "Book producer",
        "bnd" => "Binder",
        "bpd" => "Bookplate designer",
        "bsl" => "Bookseller",
        "ccp" => "Conceptor",
        "chr" => "Choreographer",
        "clb" => "Collaborator",
        "cli" => "Client",
        "cll" => "Calligrapher",
        "clt" => "Collotyper",
        "cmm" => "Commentator",
        "cmp" => "Composer",
        "cmt" => "Compositor",
        "cng" => "Cinematographer",
        "cnd" => "Conductor",
        "cns" => "Censor",
        "coe" => "Contestant -appellee",
        "col" => "Collector",
        "com" => "Compiler",
        "cos" => "Contestant",
        "cot" => "Contestant -appellant",
        "cov" => "Cover designer",
        "cpc" => "Copyright claimant",
        "cpe" => "Complainant-appellee",
        "cph" => "Copyright holder",
        "cpl" => "Complainant",
        "cpt" => "Complainant-appellant",
        "cre" => "Creator",
        "crp" => "Correspondent",
        "crr" => "Corrector",
        "csl" => "Consultant",
        "csp" => "Consultant to a project",
        "cst" => "Costume designer",
        "ctb" => "Contributor",
        "cte" => "Contestee-appellee",
        "ctg" => "Cartographer",
        "ctr" => "Contractor",
        "cts" => "Contestee",
        "ctt" => "Contestee-appellant",
        "cur" => "Curator",
        "cwt" => "Commentator for written text",
        "dfd" => "Defendant",
        "dfe" => "Defendant-appellee",
        "dft" => "Defendant-appellant",
        "dgg" => "Degree grantor",
        "dis" => "Dissertant",
        "dln" => "Delineator",
        "dnc" => "Dancer",
        "dnr" => "Donor",
        "dpc" => "Depicted",
        "dpt" => "Depositor",
        "drm" => "Draftsman",
        "drt" => "Director",
        "dsr" => "Designer",
        "dst" => "Distributor",
        "dtc" => "Data contributor ",
        "dte" => "Dedicatee",
        "dtm" => "Data manager ",
        "dto" => "Dedicator",
        "dub" => "Dubious author",
        "edt" => "Editor",
        "egr" => "Engraver",
        "elg" => "Electrician ",
        "elt" => "Electrotyper",
        "eng" => "Engineer",
        "etr" => "Etcher",
        "exp" => "Expert",
        "fac" => "Facsimilist",
        "fld" => "Field director ",
        "flm" => "Film editor",
        "fmo" => "Former owner",
        "fpy" => "First party",
        "fnd" => "Funder",
        "frg" => "Forger",
        "gis" => "Geographic information specialist ",
        "grt" => "Graphic technician",
        "hnr" => "Honoree",
        "hst" => "Host",
        "ill" => "Illustrator",
        "ilu" => "Illuminator",
        "ins" => "Inscriber",
        "inv" => "Inventor",
        "itr" => "Instrumentalist",
        "ive" => "Interviewee",
        "ivr" => "Interviewer",
        "lbr" => "Laboratory ",
        "lbt" => "Librettist",
        "ldr" => "Laboratory director ",
        "led" => "Lead",
        "lee" => "Libelee-appellee",
        "lel" => "Libelee",
        "len" => "Lender",
        "let" => "Libelee-appellant",
        "lgd" => "Lighting designer",
        "lie" => "Libelant-appellee",
        "lil" => "Libelant",
        "lit" => "Libelant-appellant",
        "lsa" => "Landscape architect",
        "lse" => "Licensee",
        "lso" => "Licensor",
        "ltg" => "Lithographer",
        "lyr" => "Lyricist",
        "mcp" => "Music copyist",
        "mfr" => "Manufacturer",
        "mdc" => "Metadata contact",
        "mod" => "Moderator",
        "mon" => "Monitor",
        "mrk" => "Markup editor",
        "msd" => "Musical director",
        "mte" => "Metal-engraver",
        "mus" => "Musician",
        "nrt" => "Narrator",
        "opn" => "Opponent",
        "org" => "Originator",
        "orm" => "Organizer of meeting",
        "oth" => "Other",
        "own" => "Owner",
        "pat" => "Patron",
        "pbd" => "Publishing director",
        "pbl" => "Publisher",
        "pdr" => "Project director",
        "pfr" => "Proofreader",
        "pht" => "Photographer",
        "plt" => "Platemaker",
        "pma" => "Permitting agency",
        "pmn" => "Production manager",
        "pop" => "Printer of plates",
        "ppm" => "Papermaker",
        "ppt" => "Puppeteer",
        "prc" => "Process contact",
        "prd" => "Production personnel",
        "prf" => "Performer",
        "prg" => "Programmer",
        "prm" => "Printmaker",
        "pro" => "Producer",
        "prt" => "Printer",
        "pta" => "Patent applicant",
        "pte" => "Plaintiff -appellee",
        "ptf" => "Plaintiff",
        "pth" => "Patent holder",
        "ptt" => "Plaintiff-appellant",
        "rbr" => "Rubricator",
        "rce" => "Recording engineer",
        "rcp" => "Recipient",
        "red" => "Redactor",
        "ren" => "Renderer",
        "res" => "Researcher",
        "rev" => "Reviewer",
        "rps" => "Repository",
        "rpt" => "Reporter",
        "rpy" => "Responsible party",
        "rse" => "Respondent-appellee",
        "rsg" => "Restager",
        "rsp" => "Respondent",
        "rst" => "Respondent-appellant",
        "rth" => "Research team head",
        "rtm" => "Research team member",
        "sad" => "Scientific advisor",
        "sce" => "Scenarist",
        "scl" => "Sculptor",
        "scr" => "Scribe",
        "sds" => "Sound designer",
        "sec" => "Secretary",
        "sgn" => "Signer",
        "sht" => "Supporting host",
        "sng" => "Singer",
        "spk" => "Speaker",
        "spn" => "Sponsor",
        "spy" => "Second party",
        "srv" => "Surveyor",
        "std" => "Set designer",
        "stl" => "Storyteller",
        "stm" => "Stage manager",
        "stn" => "Standards body",
        "str" => "Stereotyper",
        "tcd" => "Technical director",
        "tch" => "Teacher",
        "ths" => "Thesis advisor",
        "trc" => "Transcriber",
        "trl" => "Translator",
        "tyd" => "Type designer",
        "tyg" => "Typographer",
        "vdg" => "Videographer",
        "voc" => "Vocalist",
        "wam" => "Writer of accompanying material",
        "wdc" => "Woodcutter",
        "wde" => "Wood -engraver",
        "wit" => "Witness"}
      end
   

    def self.valid_child_types
      ["data", "supporting file", "profile", "lorem ipsum", "dolor"]
    end

    def to_solr(solr_doc=Hash.new)
      super(solr_doc)
        
      ::Solrizer::Extractor.insert_solr_field_value(solr_doc, Solrizer.default_field_mapper.solr_name('object_type', :facetable), "Thesis or dissertation")
      ::Solrizer::Extractor.insert_solr_field_value(solr_doc, Solrizer.default_field_mapper.solr_name('object_type', :stored_searchable), "Thesis or dissertation")
      ::Solrizer::Extractor.insert_solr_field_value(solr_doc, Solrizer.default_field_mapper.solr_name('mods_journal_title_info', :facetable), "Unknown") if solr_doc["mods_journal_title_info_facet"].nil?

      #names = extract_person_full_names_and_computing_ids
      #::Solrizer::Extractor.insert_solr_field_value(solr_doc, Solrizer.default_field_mapper.solr_name('person_full_name_cid_facet', :facetable), names)
      #::Solrizer::Extractor.insert_solr_field_value(solr_doc, Solrizer.default_field_mapper.solr_name('person_full_name_cid_facet', :stored_searchable), names)

      solr_doc
    end

    # extracts the last_name##full_name##computing_id to be used by home view
    def extract_person_full_names_and_computing_ids
      names = {}
      self.find_by_terms(:person).each do |person|
        name_parts = person.children.inject({}) do |hash,child|
          hash[child.get_attribute(:type)] = child.text if ["family","given"].include? child.get_attribute(:type)
          hash["computing_id"] = child.text if child.name == 'computing_id'
          hash
        end
        #if name_parts.length == 3 and person.search(:roleTerm).children.text.include?("Author")
        if name_parts.length == 3 and person.search(:roleTerm).children.text.include?("Author")
          if name_parts["family"].blank? && name_parts["given"].blank? && name_parts["computing_id"].blank?
          #if name_parts["family"].blank? && name_parts["given"].blank?
            value = "Unknown Author"
          else
            value = "#{name_parts["family"]}, #{name_parts["given"]} (#{name_parts["computing_id"]})"
            #value = "#{name_parts["family"]}, #{name_parts["given"]}"
          end
          #::Solrizer::Extractor.insert_solr_field_value(names, "person_full_name_cid_facet", value) if name_parts.length == 3        
          ::Solrizer::Extractor.insert_solr_field_value(names, "person_full_name_cid_facet", value)
          ::Solrizer::Extractor.insert_solr_field_value(names, Solrizer.default_field_mapper.solr_name('person_full_name_cid_facet', :facetable), value)
          ::Solrizer::Extractor.insert_solr_field_value(names, Solrizer.default_field_mapper.solr_name('person_full_name_cid_facet', :stored_searchable), value)
        end      
      end
      names
    end

end

