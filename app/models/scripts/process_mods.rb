
require 'nokogiri'
require 'date'
require 'set'
require 'find'
require 'active-fedora'

require './datastreams/mods_article_datastream.rb'


#
# This class adds new elements to the mods file and
# also cleans the existing elements as per requirement
#
class ProcessMods

      # public interface method which takes mods file as input and
      # added and cleans elements  
      def clean_mods(unzip_path, mods_file)
           
        puts 'Cleaning up MODS [' + mods_file + ']'
 
        # reade MODS file as xml instance  
        mods_xml = Nokogiri::XML(File.open(mods_file))


        # put all MODS clean up code here
        clean_author(mods_xml)
        clean_corporate(mods_xml) 
        add_aleph_sys_num(mods_xml)  
        clean_record_info(mods_xml)
        clean_origin_info(mods_xml)

	#TODO: Need to update this method to map language codes to text
        update_language_code_with_text(mods_xml)

        clean_physical_description(unzip_path, mods_xml)
        clean_physical_loc(mods_xml)
        clean_other_elements(mods_xml)
        
        # In the end, write back the modified instance to file
        format_and_write(mods_xml, mods_file)
      end		

      # addes author elements to the mods xml in a new format
      def clean_author(mods_xml)
   
        authors = mods_xml.css('//name[@type="personal"][@usage="primary"]')
        authors.each do|author|
          full_name =  author.at_css('namePart').content
            
          # extract and clean middle name  
          name_part = full_name.split ','
          l_name = name_part[0]
          fm_name = name_part[1] if name_part.length > 1
          if fm_name  
            fm_name_arr = fm_name.split
            f_name = fm_name_arr[0]
            m_name = fm_name_arr[1] if fm_name_arr.length > 1
            if m_name
 
              if m_name.size == 1
                m_name = m_name + "."
              elsif m_name.size > 2
                m_name.slice! '.'
              end

              given_name = f_name.to_s + ' ' + m_name
              given_name.strip!
            else 
             given_name = f_name
            end

          end   
         
          # get and populate person template  
          person = ModsArticleDatastream.person_template 
          person['usage'] = 'primary'          
          person.at_css('namePart[@type="family"]').content = l_name
          person.at_css('namePart[@type="given"]').content = given_name
          role_term = person.at_css('role/roleTerm[@type="text"]')
          role_term['authority'] = 'marcrelator'
          role_term.content = 'Author' 

          # replace author node
          author.add_next_sibling(person)  
          author.remove  
        end
 
      end

      # adds corporate element to the mods in a new format 
      def clean_corporate(mods_xml)
        
        corporates = mods_xml.css('//name[@type="corporate"]')
        corporates.each do |corporate|

          # remove '.' from nameParts  
          name_parts = corporate.css('namePart') 
          name_parts.each do |name_part|
             name_part.content = remove_trailing_dot(name_part.content)
          end


          # create and add role node 
          builder = Nokogiri::XML::Builder.new do |xml|
            xml.role {
              xml.roleTerm(:type=>"text",:authority=>"marcrelator")
            }
          end  

          role = builder.doc.root
          role.at_css('roleTerm[@type="text"][@authority="marcrelator"]').content = 'Degree grantor'     
          corporate.add_child(role)
            
        end 

      end

      # adds aleph number element to the mods xml 
      def add_aleph_sys_num(mods_xml)

        aleph = mods_xml.at_css('//recordInfo/recordIdentifier:not([source])')   
        if aleph
          aleph_num = aleph.content

          # create identifier node
          builder = Nokogiri::XML::Builder.new do |xml|
            xml.identifier(:type=>"system")
          end

          node = builder.doc.root
          node.content = aleph_num

          # get ref of last identifier node in mods
          identifier = mods_xml.css('//identifier').last
          if identifier  
            identifier.after(node)
          else
            mods_xml.root.last_element_child.add_child(node)
          end
 
        end  

      end

      # clean record info element as per requirement
      def clean_record_info(mods_xml)

        record_info = mods_xml.at_css('//recordInfo') 

        rec_cont_source = record_info.at_css('recordContentSource') 
        if rec_cont_source
           rec_cont_source.content = 'CU-SB'
        end 

        rec_create_date = record_info.at_css('recordCreationDate')
        if rec_create_date
           rec_create_date['encoding'] = 'iso8601'
           rec_create_date.content = DateTime.now
        end

        rec_change_date = record_info.at_css('recordChangeDate')
        if rec_change_date
         #rec_change_date.remove
         rec_change_date['encoding'] = 'iso8601'
         rec_change_date.content = ''
        end

        builder = Nokogiri::XML::Builder.new do |xml|
           xml.languageOfCataloging(:usage=>"primary"){ 
           xml.languageTerm(:type=>"code", :authority=>"iso639-2b")
           }
        end
        
        lang = builder.doc.root
        lang.at_css('languageTerm[@type="code"][@authority="iso639-2b"]').content = 'eng' 

        #rec_iden = mods_xml.at_css('//recordInfo/recordIdentifier[@source="OCoLC"]') 
        rec_iden = record_info.at_css('recordIdentifier') 
        if rec_iden
	  rec_iden.remove
          #rec_iden.add_next_sibling(lang)
        #else
          #record_info.add_child(lang)
        end

	if lang
	  record_info.add_child(lang)
	end

        rec_origin = record_info.at_css('recordOrigin')
        if rec_origin
          content = rec_origin.content 
          rec_origin.content = content.to_str + '. Following intitial conversion, elements were added' +
                              ' from ProQuest supplied XML and second stylesheet applied to comply with' + 
                              ' local MODS profile for ETDs.' 
        end    

      end

      # clean dateIssued and add dateCreated to the mods
      def clean_origin_info(mods_xml)
     
        origin_info = mods_xml.at_css('//originInfo')
        date_issued = origin_info.at_css('dateIssued')
        date_issued.name = 'dateCreated'
        date_issued['encoding'] = 'iso8601'
        date_issued['keyDate'] = 'yes'
        date_issued_marc =  origin_info.at_css('dateIssued[@encoding="marc"]')
        date_issued_marc['encoding'] = 'iso8601'    
	# clean-up date_issued with marc encoding if there is a duplicate
        date_issued_marc_enc =  origin_info.at_css('dateIssued[@encoding="marc"]')
	if date_issued_marc_enc
	  date_issued_marc_enc.remove
	end
      end

      # clean and add MIME type of the proquest attachement files
      # in the physical description
      def clean_physical_description(unzip_dir, mods_xml)

        phy_desc = mods_xml.at_css('//physicalDescription')
        extent = phy_desc.at_css('extent')
 
        # create a set of all MIME types composed in an ETD
        mime_type_set = Set.new
        Find.find(unzip_dir) do |f|

          unless File.directory?(f)
             mime_type_set.add(MIME::Types.type_for(f).first) if MIME::Types.type_for(f).first
          end
        end
 
        # add an MIME element  
        mime_type_set.each do |mime_type|
          
          imt_builder = Nokogiri::XML::Builder.new do |xml|
           xml.internetMediaType
          end
          int_media_type = imt_builder.doc.root  
          int_media_type.content = mime_type
          
          if extent
            extent.add_previous_sibling(int_media_type)
          else
            phy_desc.add_child(int_media_type)
          end
        
        end

        # build digital origin node
        do_builder = Nokogiri::XML::Builder.new do |xml|
           xml.digitalOrigin
        end
        digital_origin = do_builder.doc.root
	digital_origin.content = 'born digital'
        
        # add node
        extent.content =  remove_trailing_dot(extent.content)
        if extent
          extent.add_next_sibling(digital_origin)
        else
          phy_desc.add_child(digital_origin)
        end 

      end

      def update_language_code_with_text(mods_xml)
	#language = mods_xml.at_css('//language/languageTerm/[@type="code"]')
	language = mods_xml.at_css('//language/languageTerm')
	if language
	  language.content = language.content.gsub('eng', 'English') if language.content.include? 'eng'
	end

      end


      # clean and add location information to the mods 
      def clean_physical_loc(mods_xml)


        locations = mods_xml.css('//location')       

        locations.each do |location|

          phy_loc = location.at_css('physicalLocation')
          # move next if physical location element is not there 
          next unless phy_loc         

          phy_loc.content = 'University of California, Santa Barbara'
          shelf_loc = location.at_css('shelfLocator') 

          builder = Nokogiri::XML::Builder.new do |xml|
            xml.holdingSimple {
              xml.copyInformation {
                xml.subLocation
                 xml.shelfLocator
              }
            }
          end
          hold_simple = builder.doc.root         
          hold_simple.at_css('subLocation').content = 'Main Library'
          shelf_loc = location.at_css('shelfLocator') 
          if shelf_loc
            hold_simple.at_css('shelfLocator').content = 'Electronic Dissertations ' + shelf_loc.content.to_s  
          end
          location.add_child(hold_simple)    
   
          # if exists, remove it 
          shelf_loc.remove if shelf_loc

       end 

      end
 

      # clean other elements of the mods xml
      def clean_other_elements(mods_xml)
        
        # add usage attribute to title
        title_info = mods_xml.at_css('//titleInfo')
        title_info["usage"] = "primary"

        # process genre
        genre_marc = mods_xml.at_css('//genre[@authority="marcgt"]')
        if genre_marc
         genre_marc.content = "thesis" 
        end 

        mods_xml.css('//genre[@authority="local"]').each do|genre|
          genre.content = remove_trailing_dot(genre.content)
	  if (genre.content.include? '-')
	    genre.content = genre.content.gsub('-', ' -- ')
	  end
        end 

        # process abstract
        abstract = mods_xml.at_css('//abstract')
puts "encoding abstract1: " + abstract
        if abstract
         abstract.content = 'Not available' if abstract.content.to_s.empty?
        else
         builder = Nokogiri::XML::Builder.new do |xml|
            xml.abstract
         end
         abstract = builder.doc.root
         abstract.content = 'Not available'
         phy_desc = mods_xml.at_css('//physicalDescription')
         phy_desc.add_next_sibling(abstract) if phy_desc
puts "encoding abstract: " + abstract
        end
 
        # process location url
	loc_url = mods_xml.at_css('//location//url[@note="ProQuest full text. Restricted to UC campuses"]')
	if (!loc_url.nil? || loc_url.to_s.size > 1)
	  loc_url.remove_attribute 'displayLabel'
          loc_url.remove_attribute 'usage'
	end

        # process note
        mods_xml.css('//note').each do|note|
	  #puts mods_xml.css('//note[@type="statement of responsibility"]')
	  #if (note.attribute[1] == "altRepGroup")
	  #  note.remove_attribute 'altRepGroup'
	  #end
	  note.content = remove_trailing_dot(note.content)
        end

       # process location
       place = mods_xml.at_css('//originInfo/place/placeTerm[@type="text"]') 
       if place
           #place.content = place.content.gsub(']', '') if place.content
           place.content = '[' + place.content if place.content.include? ']'
       end        

       accessCondition = mods_xml.at_css('//accessCondition[@type="restrictionOnAccess"]')
       if accessCondition
	 accessCondition.remove 
       end

      end
    
      def add_ark(ark, mods_file)

        builder = Nokogiri::XML::Builder.new do |xml|
          xml.identifier(:type=>"ark")
        end
        iden = builder.doc.root
        iden.content = ark

        mods_xml = Nokogiri::XML(File.open(mods_file)) 
        identifier = mods_xml.css("identifier").last
        if identifier
          identifier.after(iden)
        else
	  root_child = mods_xml.root.last_element_child
	  if root_child
	    root_child.add_child(iden)
	  end
        end
        format_and_write(mods_xml, mods_file)

      end

       
      # add additional location element to the mods
      # having a url and thumbnail
      def add_location(pid, mods_file)

       builder = Nokogiri::XML::Builder.new do |xml|
          xml.location {
            xml.url(:usage=>"primary", :access=>"object in context")
            xml.url(:access=>"preview")
          }
       end
       location = builder.doc.root

       #TODO: consider reading the domain name from yml file
       primary_url = location.at_css('url[@usage="primary"]')
       #primary_url.content = "http://alexandria.ucsb.edu/fedora/objects/" + pid
       #primary_url.content = "http://alexandria.ucsb.edu/lib/" + pid
       #url = APP_CONFIG['rails_url']
       url = 'http://repodev1.library.ucsb.edu/'
       primary_url.content = url + "lib/" + pid

       preview_url = location.at_css('url[@access="preview"]')
       #preview_url.content = "http://alexandria.ucsb.edu/fedora/objects/" + pid + "/thumbnail"
       #preview_url.content = "http://alexandria.ucsb.edu/lib/" + pid + "/thumbnail"
       preview_url.content = url + "lib/" + pid + "/thumbnail"

       mods_xml = Nokogiri::XML(File.open(mods_file))
       loc = mods_xml.css("location").last
       if loc
         loc.after(location)
       else
         mods_xml.root.add_child(location)
       end
       format_and_write(mods_xml, mods_file)  

      end

      # a utility method which takes the xml and destination file as input.
      # formats the xml before writing it to the file
      def format_and_write(xml, file)
        formatted_xml = Nokogiri::XML(xml.to_xml) { |x| x.noblanks }
        File.open(file, 'w') do |f|
        f.puts formatted_xml
        end
      end

      # utility method to remove trailing dot from a string
      def remove_trailing_dot(str)
          if !str || str.size == 1
             return str
          end
          str.strip!
          str = str[0, str.size-1] if str[-1] == '.'
          str
      end


end


#MAIN

#m = ProcessMods.new
#m.clean_mods('mods/Winkler_ucsb_0035_11066.pdf.xml')

#END




