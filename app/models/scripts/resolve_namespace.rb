
require 'nokogiri'

class ResolveNamespace


    def self.resolve_mods_namespace(mods_file)
     
      puts 'Resolving MODS namespace...'
 
      mods_xml = Nokogiri::XML(File.open(mods_file))
      mods_xml.root.add_namespace 'mods', 'http://www.loc.gov/mods/v3'
      #mods_xml.root.add_namespace '', ''

      # adding namespace prefix to root element first
      mods_xml.root.namespace = mods_xml.root.namespace_definitions.find{|ns| ns.prefix == "mods"}      
 
      mods_xml.root.children.each do |node|
          add_namespace_prefix(node, mods_xml)
      end

      File.open(mods_file, 'w') do |f2|
       f2.puts mods_xml
      end 
    
    end  

     
    # add name space prefix recursively
    def self.add_namespace_prefix(node, mods_xml)

      if node.nil?
        return nil
      end

      node.namespace = mods_xml.root.namespace_definitions.find{|ns| ns.prefix == "mods"}

      node.children.each do |child|

        # skip degree element which is in other namespace
        if child.name == "degree"
          next
        end

        child.namespace = mods_xml.root.namespace_definitions.find{|ns| ns.prefix == "mods"}
        add_namespace_prefix(child, mods_xml)
      end

    end



end


#MAIN

#ResolveNamespace.resolve_mods_namespace('Winkler_ucsb_0035_11066.pdf.xml')


