#fedora_ingest.rb

require 'active-fedora'
require 'datastreams/mods_article_datastream.rb'
require 'etd.rb'
require 'ezid_adapter.rb'
require 'logging.rb'
require 'process_mods.rb'
require 'resolve_namespace.rb'
require 'process_rightsmetadata.rb'
class FedoraService

    # enable logging capability 
    include Logging

     def initialize()
        #create an array of JSON objects
        @json_obj = %w[]
        @json_obj.to_a()
	@adrl_url = APP_CONFIG['adrl_url']
	@ark_url = APP_CONFIG['ark_url']
     end

    # mint ark from EZID external interface
    def retrieve_ark()

       # mint ark from EZID
       #@config = ConfigReader.new('ingest.yml')
       @config = ConfigReader.new('config/ingest.yml')
       ez_adapter = EzidAdapter.new(@config)
       ark = ez_adapter.mint_ark_from_ezid()
	puts "retrieve_ark: " + ark
       ark

    end

    # public interface method which which ingest the 
    # processed mods and attachements into the fedora
    # repository. it also writes writes the success or
    # error result to the ingest.log file
    def ingest(content_path, mods_file, pattern)

       message = ""
       begin  
         ark  = retrieve_ark() 
         pid = get_pid(ark)
	 puts "ingest: pid: " + pid
         create_object('ETD', pid)     
         add_datastreams(content_path, pattern)
         add_metadatastreams(mods_file)
	 #method call for access controls
         add_rights_metadatastream(mods_file, content_path) 
         add_additional_prop(pattern)
         @etd.save
         #ingest_logger.info 'Successfully Ingested object [ARK: ' + ark + ']'
	 message = 'Successfully Ingested object [ARK: ' + ark + ']' 
         ingest_logger.info message
       rescue => exception
         #ingest_logger.error 'Error while ingesting object [ARK: ' + ark + ']' + 
         #                     '\nError Details: ' + exception.backtrace.join("\n\t")
	 if !(ark.nil? && ark.blank?)
	   message = 'Error while ingesting object [ARK: ' + ark + ']' + '\nError Details: ' + exception.backtrace.join("\n\t")
           ingest_logger.error message
	 else
	   msg_a = %w[]
	   msg_a.to_a()
	   #message = 'Error in minting a new ark' + '\nError: ' + exception.to_s + '\nError Details: ' + exception.backtrace.join("\n\t")
	   msg_a.push('Error in minting a new ark' + '\nError: ')
	   msg_a.push(exception)
	   msg_a.push('\nError Details: ' + exception.backtrace.join("\n\t"))
	   message = msg_a
           ingest_logger.error message
	 end

         #ingest_logger.error message
       ensure
         #log_object(mods_file, ark)  
         log = log_object(mods_file, ark, message)

	     @json_obj.push(log)
         #puts "json_obj"
         #puts @json_obj
       end

       return pid
    end


   def get_log()
     return @json_obj
   end 

   # get ark id after adding institute id to it
   def get_pid(ark)
      # remove '/<institute_id>/' from  ark
      institute_id = @config.get_value('institute_id')
      pid = ark.gsub('/' + institute_id.to_s + '/', '')

      # replace ark with adrl
      pid = pid.gsub(@ark_url, @adrl_url)

      pid
   end 

   # create object as per the provided type 
   def create_object(type, pid)
      if type == 'ETD'
        # initialize active fedora with local config
        ActiveFedora.init(:fedora_config_path => 'config/fedora.yml')
        @etd = ETD.create(:pid => pid)
        puts "ETD type object created [PID: " + pid + "]"
      end
   end

   # add main datastream and supplementary files to the 
   # object 
   def add_datastreams(content_path, pattern)
      count = 0
      Find.find(content_path) do |f|

        name = File.basename(f)
        if File.fnmatch(pattern + '*.pdf', name)
          puts "Ingesting pdf file " + name
          @etd.dataContent.content = File.open(f)

          #set dsLabel to only file name without extension
          file_ext_name = File.extname(f)
          puts "file_ext_name: " + file_ext_name.to_s()
          file_name = File.basename(f, file_ext_name)
          puts "file_name: " + file_name.to_s()

          @etd.dataContent.dsLabel = file_name
          #@etd.save

        elsif !File.directory?(f) && !File.fnmatch(pattern + '*.xml', name)
          puts "Ingesting supp file " + name

          #set dsLabel to only file name without extension
          file_ext_name = File.extname(f)
          puts "file_ext_name: " + file_ext_name.to_s()
          file_name = File.basename(f, file_ext_name)
          puts "file_name: " + file_name.to_s()
          
          # commented for testing of failure to ingest objects
          # @etd.add_datastream(etd.create_datastream(ActiveFedora::Datastream, "suppContent#{count}", :dsLabel => name))
          # @etd.add_datastream(@etd.create_datastream(ActiveFedora::Datastream, "suppContent#{count}", :dsLabel => name))
          @etd.add_datastream(@etd.create_datastream(ActiveFedora::Datastream, "suppContent#{count}", :dsLabel => file_name))
          @etd.datastreams["suppContent#{count}"].content = File.open(f)
          count += 1
          #@etd.save

        end

      end

   end

   # add mods xml to the stream after resolving the 
   # namespace and prefix
   def add_metadatastreams(mods_file)

      pid = @etd.pid
      puts "Ingesting MODS xml " + mods_file
     
      # add location and ark id to mods  
      mo = ProcessMods.new
      institute_id = @config.get_value("institute_id")

      #mo.add_ark(pid.gsub(":",":/" + institute_id.to_s + "/"), mods_file)
      pid = pid.gsub(":",":/" + institute_id.to_s + "/")
      pid = pid.gsub(@adrl_url, @ark_url)
      mo.add_ark(pid, mods_file)

      #mo.add_location(pid, mods_file)
      #mo.add_location(pid.gsub(":",":/" + institute_id.to_s + "/"), mods_file)
      mo.add_location(pid, mods_file)

      # resolve namespace
      ResolveNamespace.resolve_mods_namespace(mods_file)

      @etd.descMetadata.content = File.open(mods_file)
      #mods_name = File.basename(mods_file)
      #mods_name.slice! '.pdf'
      
      #code for changing mods file name
      #puts institute_id.to_s()
      #puts pid.to_s()
      #new_pid = pid.to_s().from(4)
      new_pid = pid.sub(':/','_')
      new_pid = new_pid.sub('/','_')
      #puts new_pid.to_s()
      #mods_name = 'ark_' + institute_id.to_s() + '_' + new_pid.to_s() + '_mods'
      mods_name = new_pid.to_s() + '_mods'
      #puts "mods_name: " + mods_name
      
      @etd.descMetadata.dsLabel = mods_name

   end

   # add rights metdatastream and default permissions
   def add_rights_metadatastream(mods_file, content_path)
     puts "Ingesting RightsMetadata"
     # read permission is added for staff to enable download and read access
#     @etd.permissions_attributes = [{:type=>"group", :access=>"discover", :name=>"admin"},
#                                    {:type=>"group", :access=>"discover", :name=>"staff"},
#                                    {:type=>"group", :access=>"read", :name=>"admin"},
#                                    {:type=>"group", :access=>"edit", :name=>"admin"}]

#     @etd.permissions_attributes = [{:type=>"group", :access=>"discover", :name=>"public"},
#                                    {:type=>"group", :access=>"read", :name=>"community"},
#                                    {:type=>"group", :access=>"edit", :name=>"staff"},
#                                    {:type=>"group", :access=>"delete", :name=>"admin"}]

     @etd.permissions_attributes = [{:type=>"group", :access=>"discover", :name=>"public"},
                                    {:type=>"group", :access=>"read", :name=>"community"},
                                    {:type=>"group", :access=>"edit", :name=>"staff"}]

     proquest_file = '' 
     Find.find(content_path) do |f|

        name = File.basename(f)
        if File.fnmatch('*_DATA.xml', name)
           proquest_file = f
           break                         
        end 
     end
     
     rm = ProcessRightsMetadata.new
     processed_rm = rm.process_rights_metadata(proquest_file, @etd.rightsMetadata.to_xml)
     processed_rm_xml = processed_rm ? processed_rm.to_xml : @etd.rightsMetadata.to_xml

     #puts "processed_rm_xml"
     #puts processed_rm_xml
     rm_with_access = rm.add_additional_access(processed_rm_xml)
     processed_rm_with_access = rm_with_access ? rm_with_access.to_xml : @etd.rightsMetadata.to_xml
     @etd.rightsMetadata.content = rm_with_access.to_xml if rm_with_access
     #@etd.permissions_attributes = [{:type=>"group", :access=>"delete", :name=>"admin"}]
     #puts "rm_with_access"
     #puts rm_with_access
     #puts "processed_rm_with_access"
     #puts processed_rm_with_access

     #rm_with_cp = rm.add_copyright(mods_file, processed_rm_xml)
     rm_with_cp = rm.add_copyright(mods_file, processed_rm_with_access)
     @etd.rightsMetadata.content = rm_with_cp.to_xml if rm_with_cp
     #puts "rm_with_cp"
     #puts rm_with_cp

   end

   # add properties stream for additional elements
   # which are not part of mods
   def add_additional_prop(pattern)
      @etd.properties.content_file = pattern + '.pdf'

      pid = @etd.pid
      institute_id = @config.get_value("institute_id")
      pid = pid.gsub(":",":/" + institute_id.to_s + "/")
      pid = pid.gsub(@adrl_url, @ark_url)
      @etd.properties.ark = pid
   end 

   def log_object(mods, ark, message)
   
     mods_xml = Nokogiri::XML(File.open(mods))
   
     # title
     title_info = mods_xml.at_css('//titleInfo[@usage="primary"]')
     if title_info 
       initial  = title_info.at_css('nonSort').content if title_info.at_css('nonSort')
       title = title_info.at_css('title').content if title_info.at_css('title')
       sub_title = title_info.at_css('subTitle').content if title_info.at_css('subTitle')
     end
     full_title = initial.to_s + title.to_s + sub_title.to_s

     #author
     name = mods_xml.css('//name[@type="personal"][@usage="primary"]/namePart')
     author = name[0].content.to_s + ', ' + name[1].content.to_s
       
     # aleph id
     system = mods_xml.at_css('//identifier[@type="system"]') 
     if system 
       aleph = system.content
     end  

     # current date 
     date = DateTime.now

     ingest_hash = {

         "Ark"      => "#{ark}",
         "Aleph"    => "#{aleph}",
         "Author"   => "#{author}",
         "Title"    => "#{full_title}",
         "Date"     =>  "#{date}"
     }

    #ingest_logger.info JSON.pretty_generate(ingest_hash)
    obj = JSON.pretty_generate(ingest_hash)
    #puts JSON.parse obj 
    ingest_logger.info obj

    #check hash values
    # puts "ingest_hash values"
    # puts ingest_hash["Ark"]

    #Adding message to hash values for retrieval in the ingest results page
    ingest_hash["Message"] = "#{message}"
    #hash = JSON.parse obj
    #puts hash

    #return obj 
    #return hash 
    return ingest_hash 

   end
 
     
end

# MAIN
#f = FedoraService.new
#f.log_object('mods/Winkler_ucsb_0035_11066.pdf.xml', 'ark:/99999/fk4cr7zfk')

# END

