

require 'rest_client'
require 'config_reader.rb'

class RestClientWrapper

    def initialize(config, cxtUrlKey, userKey, pwdKey)
      @config = config
      @cxtUrl = @config.get_value(cxtUrlKey)
      @user = @config.get_value(userKey)
      @pwd = @config.get_value(pwdKey)
      @env_yml = APP_CONFIG['ucsb_env']
      @proxy_yml = APP_CONFIG['http_proxy']
    end

    def post(uriKey)

      
       url = construct_url(uriKey)  
       
       puts "Minting ARK ID from EZID..."

# Code below is to avoid public ips on dev server
#if (@env_yml == "development")
#  RestClient.proxy = APP_CONFIG['http_proxy']
#  puts "RestClient.proxy inside if"
#  puts RestClient.proxy
#end

       resource = RestClient::Resource.new url, @user, @pwd
       
       # Fire 
       res = resource.post "", :content_type => 'text/plain'   

       case res.code

       when 201

         puts "Successfully minted ARK ID from EZID"
         puts res.description
         res
       
       else 

         puts "Error while minting ARK from EZID"
         puts res.description
         raise RestClient::ExceptionWithResponse.new(response = res, initial_response_code = res.code) 
       end
    end 


    def construct_url(uriKey)
     return File.join(@cxtUrl, @config.get_value(uriKey))	
    end

    def get(uriKey)
      # implement later
    end

    def put(uriKey)
      # implement later
    end

end



#MAIN

#config = ConfigReader.new('ingest.yml')
#rest = RestClientWrapper.new(config, 'ezid_url', 'ezid_user', 'ezid_password')
#rest.post('ezid_post_uri')
#rest.get('ezid_get_uri')


#END


