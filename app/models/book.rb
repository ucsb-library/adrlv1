class Book < ActiveFedora::Base
  has_metadata 'descMetadata', type: BookMetadata

  delegate :title, to: 'descMetadata', multiple: false
  delegate:author, to: 'descMetadata', multiple: false

end

