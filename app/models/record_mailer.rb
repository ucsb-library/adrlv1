# -*- encoding : utf-8 -*-
# Only works for documents with a #to_marc right now. 
class RecordMailer < ActionMailer::Base
  def email_record(documents, details, url_gen_params)
    #raise ArgumentError.new("RecordMailer#email_record only works with documents with a #to_marc") unless document.respond_to?(:to_marc)
        
    subject = I18n.t('blacklight.email.text.subject', :count => documents.length, :title => (documents.first.to_semantic_values[:title] rescue 'N/A') )

    @documents      = documents
    @message        = details[:message]
    @url_gen_params = url_gen_params

    #puts @url_gen_params

    @url = APP_CONFIG['rails_url']
    @object_url = %w[]
    @object_url.to_a()

    count = @documents.length
    (0..count-1).each do |s|
	object_id = @documents[s].id
	#@object_url = @url.to_s + 'catalog/' + @object_id
	@object_url.push(@url.to_s + 'catalog/' + object_id)
    end

    @message_body = details[:message] + "</br> Your selected items from Alexandria Digital Research Library are below: </br>"
    (0..count-1).each do |m|
	@message_body += @object_url[m].to_s + "</br>"
    end

    #mail(:to => details[:to],  :subject => subject)
    #mail(to: details[:to], body: details[:message], content_type: "text/html", :subject => subject)
    mail(to: details[:to], body: @message_body, content_type: "text/html", :subject => "ADRL Selected Items")
  end
  
  def sms_record(documents, details, url_gen_params)
    @documents      = documents
    @url_gen_params = url_gen_params
    mail(:to => details[:to], :subject => "")
  end

  #default from: 'sheetal.chavan@experis.com'
  @url = APP_CONFIG['rails_url']
  @from_email = APP_CONFIG['from_email']
  default from: @from_email

  #def ucsb_email1(toe, frome, messagee)
#  def email_record(toe, messagee)
#        toe="sheetal.chavan@experis.com"
#    mail(to: toe, body: messagee, content_type: "text/html", subject: "Welcome to ADRL!")
#  end


end
