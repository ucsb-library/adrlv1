
require 'marc'
require 'nokogiri'
require 'find'
require 'zip'

require 'logging.rb'

class MatchingZipProcessor

    # enable logging capability
    include Logging 

     def initialize()
        #create an array of JSON objects
        @skipped_obj = %w[]
        @skipped_obj.to_a()
     end

     def get_log()
	return @skipped_obj
     end 

    def find_by_author_title(marcFile, zipPath)

       xsl = "xslt/MARC21slim2MODS3-4.xsl"

        marcxmlFile = "marcxml/marc.xml"
        #convert MARC to MARCXML record
        MARC::XMLReader.best_available!
        marc_2_marcxml(marcFile, marcxmlFile)

        # reading a marcxml record
        reader = MARC::XMLReader.new(marcxmlFile)
        for record in reader

          # read PDF name
          field_title  = record["245"].find{|subfield| subfield.code =='a'}
          title = field_title.value
          field_author = record["100"].find{|subfield| subfield.code =='a'}
          author = field_author.value
          logger.debug '**********Start Searching********'

          process_matching_zip(zipPath, title, author)

          logger.debug '**************End*****************'
          logger.debug get_attached_content_file
          logger.debug get_matching_zip_file  
        end 


    end 

    #def process_matching_zip(zipPath, title, author)
    def process_matching_zip(zipPath, title, author, system_number)

       # hash map to keep the values genetrated while finding matching
       # zip file   
       @hash = Hash.new
       zip = find_matching_zip(zipPath, title, author)
       puts "zip inside process_matching_zip"
	puts zip	
       if (zip.nil? || zip.blank?)
	 skipped_message = ""
         if !system_number.to_s.nil? && !system_number.to_s.blank?
	   skipped_message = "MARC Record <" + system_number.to_s() + "> was not ingested. No matching Proquest Zip file found."
	   log_skipped_message(skipped_message)
	 elsif !author.nil? && !author.blank?
	   skipped_message = "MARC Record for author: <" + author.to_s() + "> was not ingested. No matching Proquest Zip file found."
	   log_skipped_message(skipped_message)
	 elsif !title.nil? && !title.blank?
	   skipped_message = "MARC Record for title: <" + title.to_s() + "> was not ingested. No matching Proquest Zip file found."
	   log_skipped_message(skipped_message)
         end
       end
       @hash['MATCHING_ZIP_FILE'] = zip if zip
    end 

    def log_skipped_message(skipped_message)
      @skipped_obj.push(skipped_message)
      ingest_logger.error skipped_message
      puts skipped_message
    end 

    def get_attached_content_file
       if @hash
         @hash['CONTENT_FILE']
       else
         logger.error 'process_matching_zip needs to be invoked before calling this method'
       end    
    end 


    def get_matching_zip_file
      if @hash
        @hash['MATCHING_ZIP_FILE']
      else
        logger.error 'process_matching_zip needs to be invoked before calling this method'
      end    
    end  

    def marc_2_marcxml(marcFile, marcxmlFile)

       # target MARCXML file to be generated
       writer = MARC::XMLWriter.new(marcxmlFile)

       # reading records from MARC file
       reader = MARC::Reader.new(File.new(marcFile, "r:binary:binary"),
                                 :external_encoding => "cp866",
                                 :internal_encoding => "utf-8")
       for record in reader
         # writing to MARCXML file
         writer.write(record)
       end

       # close MARCXML file
       writer.close()

    end


    def find_matching_zip(path, title, author)

     Find.find(path) do |f|

      ext = File.extname(f)
      if ext == '.zip' && is_matching_zip(f, title, author)
         return File.basename(f)
      end
     end

    end


    def is_matching_zip(file, title, author)
     Zip::File.open(file) { |zip_file|
       zip_file.each { |f|
         if File.fnmatch('*_DATA.xml', File.basename(f.name))
           found = find_in_proquest_xml(f.get_input_stream(), title, author) 
           return found if found
         end
       }
     }
     return false
   end

   def find_in_proquest_xml(pro_file, title, author)
                
      pro_xml = Nokogiri::XML(pro_file)
      #pro_xml = Nokogiri::XML(File.open("Shishim_ucsb_0035D_11180_DATA.xml"))
      pro_title = pro_xml.xpath('//DISS_description/DISS_title').text
      title = title.downcase
      pro_title = pro_title.downcase
      if (title == pro_title) || (title.include? pro_title) || (pro_title.include? title)
         logger.debug "Match found for Title"
          
      else
         logger.debug 'Match not found for Title [Marc: "' + title + '" , Proquest: "' + pro_title + '"]'
         return false
      end

      # process author first and last name from marc author
      if (!author || author.size > 1)
	puts "Author" + author
        auth_arr = author.split ','
        l_name = auth_arr[0]
        fm_name_arr = auth_arr[1].split if auth_arr.length > 1
        f_name = fm_name_arr[0]
        f_name = f_name.gsub('.', '') if f_name.size > 2
        #m_name = fm_name_arr[1] if auth_arr.length > 1   
      end	

      # read author and title from proquest  
      name = pro_xml.xpath('//DISS_author/DISS_name') 
      pro_l_name = name.xpath('DISS_surname').text
      pro_f_name = name.xpath('DISS_fname').text
      #pro_m_name = name.xpath('DISS_middle').text

      if !l_name.nil? && l_name.casecmp(pro_l_name) == 0
        logger.debug 'Match found for Last name'
      else
	 #if (!l_name.to_s.nil? && !l_name.to_s.blank?)
	 if (!l_name.nil? && !l_name.blank?)
           logger.debug 'Match not found for Last name [Marc: "' + l_name + '" , Proquest: "' + pro_l_name + '"]'
         else
           logger.debug 'Match not found for Last name [Proquest: "' + pro_l_name + '"]'
         end
        return false 
      end

      if !f_name.nil? && f_name.casecmp(pro_f_name) == 0
        logger.debug 'Match found for First name'
      else
        logger.debug 'Match not found for First name [Marc: "' + f_name + '" , Proquest: "' + pro_f_name + '"]'
        return false
      end

      #if !m_name.nil? && m_name.casecmp(pro_m_name) == 0
      #  puts 'Match found for Middle name'
      #else
      # puts 'Match not found for Middle name [Marc: "' + m_name.to_s + '" , Proquest: "' + pro_m_name.to_s + '"]'
      #  return false
      #end

      # We have reached here means matching zip is found
      content_file = pro_xml.xpath('//DISS_content/DISS_binary[@type="PDF"]')
      @hash['CONTENT_FILE'] =  content_file.text if content_file

      true


   end


end


#MAIN

#f = MatchingZipProcessor.new
#f.find_by_author_title("marc/ETDsampleExport_rev2.mrc", "etd/")
#f.find_by_author_title("marc/ETDSampleExport_Batch2.mrc", "etd/")


#END



