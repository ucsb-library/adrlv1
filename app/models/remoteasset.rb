class RemoteAsset < ActiveFedora::Base
  has_metadata :name=>'content', :control_group=>'E', :type=>ActiveFedora::Datastream
end

@asset = RemoteAsset.new
@asset.content.dsLocation = "/root/rails/sc_hydra_head/app/models/datastreams/sample_ingest_data/etdadmin_upload_133770/"
@asset.save
