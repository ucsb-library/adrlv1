

require 'logger'

# A common logging module in which multiple loggers could be defined
# just include it in the ruby file and call appropriate method to log 
# to the log file 
module Logging

  LEVEL = Logger::DEBUG 

  def logger
    Logging.logger
  end

  def self.logger
    @logger ||= Logger.new(STDOUT) 
  end 

  def ingest_logger
    Logging.ingest_logger
  end 

  def self.ingest_logger
    if !@ingest_logger  
     @ingest_logger = Logger.new('ingest.log', 20, 1024000)
     @ingest_logger.formatter = proc do | severity, datetime, progname, msg|
             "UCSB_INGEST: #{datetime} [#{progname}] #{severity} - #{msg} \n"
     end 	 
     @ingest_logger.level = LEVEL
    end  
    @ingest_logger 
  end


end

