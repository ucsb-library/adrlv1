require 'marc'
require 'nokogiri'
require 'find'
require 'zip'


require 'active-fedora'
require 'datastreams/mods_article_datastream.rb'
require 'datastreams/rights_metadata.rb'
require 'etd.rb'
require 'process_proquest.rb'
require 'process_mods.rb'
require 'fedora_service.rb'
require 'solr_service.rb'
require 'logging.rb'
require 'matching_zip_processor.rb'

class Main

     include Logging

     def initialize()
	#create an array of pids
	@main_pids = %w[]
	@main_pids.to_a()
        #create an array of JSON log objects
        @hist_log= %w[]
        @hist_log.to_a()
        @duplicate_rec= %w[]
        @duplicate_rec.to_a()
        @skipped_rec= %w[]
        @skipped_rec.to_a()
     end

     def batch_injest(marc_file, zip_path)

    
        xsl = "xslt/MARC21slim2MODS3-4.xsl"

	marcxml_file = "marcxml/marc.xml"	
        #convert MARC to MARCXML record
        MARC::XMLReader.best_available!
	    marc_2_marcxml(marc_file, marcxml_file)

  	# reading a marcxml record
  	reader = MARC::XMLReader.new(marcxml_file)
  	for record in reader
          
          # extract system number from marc record for logging skipped records 
	  record_001 = record["001"]
          system_number = ""
	  if (!record_001.nil? || !record_001.blank?)
            system_number = record_001.value
	  end

          # extract title and author from marc record  
	  record_245 = record["245"]
          title = ""
	  if (!record_245.nil? || !record_245.blank?)
            field_title  = record["245"].find{|subfield| subfield.code =='a'}
            title = field_title.value
	  end

	  #TODO: Read value from external file and read the values and check for null and blank values
	  record_100 = record["100"]
          author = ""
	  if (!record_100.nil? || !record_100.blank?)
            field_author = record["100"].find{|subfield| subfield.code =='a'}
            author = field_author.value
	  end

          # find matching zip and pdf name
          zip_processor = MatchingZipProcessor.new

	  #if (!author || author.to_s.size > 1)
	  #  puts "author in main" + author + "end"
          #end

	  #zip_processor.process_matching_zip(zip_path, title, author)
	  zip_processor.process_matching_zip(zip_path, title, author, system_number)
          pdf_name = ""
          pdf_name = zip_processor.get_attached_content_file
          matching_zip = zip_processor.get_matching_zip_file 

	  if (!pdf_name.nil? || pdf_name.to_s.size > 1)
	    puts "Preparing to ingest record [PDF: " + pdf_name + "]"
	  end

          #matching_zip = zip_processor.get_matching_zip_file 

	  #TODO: Uncomment this code to disable duplicate records insertion
          # revoke ingestion of duplicate objects
          # query solr with pdfname as primary key
	  if (!pdf_name.nil? || pdf_name.to_s.size > 1)
            solr = SolrService.new
            if solr.does_index_exists("content_file_tesim",  '"' + pdf_name + '"')
		if !(system_number.nil? || system_number.blank?)
		  @duplicate_rec.push("MARC record <" + system_number.to_s() + "> was not ingested. This ETD is already in the repository.")
		else
	     	  @duplicate_rec.push("<" + pdf_name.to_s() + "> was not ingested. This ETD is already in the repository.")
		end
               puts "Record already exists!!"
               ingest_logger.info "The record already exists [PDF: " + pdf_name + "]"
               next
            end

          single_marcxml_file = "marcxml/" + pdf_name + ".xml"
#puts "single_marcxml_file: marcxml/" + pdf_name + ".xml: " + single_marcxml_file 
		  #TODO: Cleanup after mods namespace integration
          #writer = MARC::XMLWriter.new(singleMarcxmlFile) 
          #writer = MARC::XMLWriter.new("marcxml/writer" + pdf_name + ".xml") 
          #writer.write(record)
          #writer.close

          # puts  single marc record in a xml
#	writer = "marcxml/writer_" + pdf_name + ".xml"
#	puts "writer: " + writer
#          File.open(writer, 'w') do |f2|
#            f2.puts record.to_xml
#          end

          # puts  single marc record in a xml  
          File.open(single_marcxml_file, 'w') do |f2|
            f2.puts record.to_xml
#puts "single marc record: " + record.to_s
          end

#puts "before calling marcxml_2_mods: " + single_marcxml_file

          single_mods_file = "mods/" + pdf_name + ".xml"
          # convert this MARC record to MODS  
          marcxml_2_mods(single_marcxml_file, xsl, single_mods_file)

	  # end loop for checking pdf_name
	  end

          # find matching Proquest Zip  
          #matching_zip = find_matching_zip(zip_path, pdf_name)

	  if matching_zip != nil
           
	   puts "MATCH FOUND [zip: " + matching_zip + ", pdf: " + pdf_name + "]"

           unzip_dir = zip_path + File.basename(matching_zip, ".zip")
           # unzip matching proquest
           unzip_file(zip_path + matching_zip, unzip_dir)

           # process proquest metadata
           proquest_xml = find_proquest_xml(unzip_dir, File.basename(pdf_name, '.pdf'))
           pro = ProcessProquest.new
           pro.proquest_2_mods(proquest_xml, single_mods_file)

           # clean up MODS
           mo = ProcessMods.new
           mo.clean_mods(unzip_dir, single_mods_file)

           # ingest object
           ing = FedoraService.new
	   tmp_pids = ing.ingest(unzip_dir, single_mods_file, File.basename(pdf_name, '.pdf'))

	   @main_pids.push(tmp_pids)

	   hist = ing.get_log()
	   #puts "hist"
	   #puts hist
	   @hist_log.push(hist)

	   #Add script to delete unzip_dir 
	   #Add code to move unzip_dir
	   # check_processed_dir_exists checks if the directory for processed files exists. 
	   # If not then it creates a processed directory.
           # unzip_dir = zip_path + File.basename(matching_zip, ".zip")
	   @processed_dir_path = (zip_path + "processed")
	   check_processed_dir_exists(@processed_dir_path)

	   # move processed matching_zip file and unzip_dir to processed directory which resides under zip_path
	   move_processed_files(zip_path+matching_zip, unzip_dir, @processed_dir_path)

	   #skip = zip_processor.get_log()
	   #@skipped_rec.push(skip)

	  # matching zip not found but pdf name is available 
          elsif (!pdf_name.nil? || pdf_name.to_s.size > 1)
           # We should never reach here but is possible
           puts "MATCHING ZIP NOT FOUND [" + pdf_name + "]"

	  # matching zip and pdf name are not available 
          elsif 
	   skip = zip_processor.get_log()
	   @skipped_rec.push(skip)
           # We should never reach here but is possible
           puts "MATCHING ZIP NOT FOUND"
          end 

        end

	puts "final main_pids list"
	puts @main_pids

	puts "final hist_log list"
	puts @hist_log
	
	puts "final skipped_rec list"
	puts @skipped_rec
	
	return @main_pids

     end

     def get_ingest_log()
	return @hist_log
     end

     def get_duplicate_recs()
	return @duplicate_rec
     end

     def get_skipped_recs()
	return @skipped_rec
     end

     def marc_2_marcxml(marc_file, marcxml_file)

       # target MARCXML file to be generated 
       writer = MARC::XMLWriter.new(marcxml_file)

       # reading records from MARC file
#       reader = MARC::Reader.new(File.new(marc_file, "r:binary:binary"),
#                                 :external_encoding => "cp866",
#   			         :internal_encoding => "utf-8")
       reader = MARC::Reader.new(File.new(marc_file, "r:binary:binary"),
                                 :external_encoding => "utf-8",
   			         :internal_encoding => "utf-8")
       for record in reader
         # writing to MARCXML file
         writer.write(record)
#puts "marc_2_marcxml: record: " + record.to_s
       end

       # close MARCXML file
       writer.close()
 
    end



    def marcxml_2_mods(src_xml, style, tgt_file)

       doc   = Nokogiri::XML(File.read(src_xml))
#	puts "marcxml_2_mods: doc: "
#	puts doc
       xslt  = Nokogiri::XSLT(File.read(style))

       # Transform MARCXML to MODS
       response = xslt.transform(doc)
       File.open(tgt_file, 'w') do |f2|
        f2.puts response
       end

#	puts "marcxml_2_mods: tgt_file: "
#	puts tgt_file

    end

    
    #@depricated
    def find_matching_zip(path, marc_pdf) 
  
     Find.find(path) do |f|

      ext = File.extname(f)
      # Unzip the file
      if ext == '.zip'
        if  is_matching_zip(f, marc_pdf) == "true"
  	 return File.basename(f)
        end
      end
     end

    end

    #@depricated
    def is_matching_zip(file, pdf_name)
     Zip::File.open(file) { |zip_file|
       zip_file.each { |f|
         if File.basename(f.name) == pdf_name
            return "true"
         end
       }
     } 
     return "false"
   end
 


    def unzip_file(file, destination)
     Zip::File.open(file) { |zip_file|
       zip_file.each { |f|
         f_path=File.join(destination, f.name)
	 FileUtils.mkdir_p(File.dirname(f_path))
         zip_file.extract(f, f_path) unless File.exist?(f_path)
       }
     }
    end

    def find_proquest_xml(src_path, pattern)

     Find.find(src_path) do |f|

      ext = File.extname(f)
      if File.fnmatch(pattern + '*.xml', File.basename(f)) 
         return File.path(f)
      end
     end
    end

    def check_processed_dir_exists(proc_dir)
      FileUtils.mkdir_p(proc_dir) unless File.directory?(proc_dir)
    end

    def move_processed_files(zip_file, zip_dir, proc_dir)
      FileUtils.mv(zip_file, proc_dir, {:force => true})

      # check if unzip dir already exists. If unzip dir is present under processed dir then first delete existing unzip dir.
      dir_check = File.directory?(proc_dir + '/' + File.basename(zip_dir))
      if (dir_check)
        FileUtils.rm_rf(proc_dir + '/' + File.basename(zip_dir))
      end

      # After deletion, move the newly created unzip dir to processed dir.
      FileUtils.mv(zip_dir, proc_dir, {:force => true})
    end

end




# MAIN

#puts "MAIN"

#marc = MAIN.new

#marc.batch_injest("marcxml/ETDsampleExportMARCXML-ver2.xml", "etd/")
#marc.batch_injest("marc/ETDsampleExport_rev2.mrc", "etd/")

# END

