require 'hydra'
require 'active-fedora'
#require 'datastreams/rights_metadata.rb'

class ETD < ActiveFedora::Base

    #TODO: consider better way of adding the below mentioned path to LOAD_PATH
    $: << "/usr/local/rvm/gems/ruby-2.0.0-p247/gems/hydra-access-controls-6.4.1/app/models"
    $: << "/usr/local/rvm/gems/ruby-2.0.0-p247/gems/hydra-access-controls-6.4.1/app/models/concerns"

    include Hydra::AccessControls::Permissions

    include Hydra::ModelMixins::RightsMetadata
    has_metadata "rightsMetadata", type: Hydra::Datastream::RightsMetadata
#    has_metadata "rightsMetadata", type: RightsMetadata

    # metadata
    has_metadata 'descMetadata', type: ModsArticleDatastream

    has_metadata 'properties', type: ActiveFedora::SimpleDatastream do |m|
      m.field "content_file", :text
      m.field "ark", :text
      #suppress_reason will be displayed only when an object is suppressed.
      m.field "suppress_reason", :text
    end

    # datastreams
    has_file_datastream 'dataContent'
    has_file_datastream 'suppContent' 

    #TODO: DEPRECATION WARNING: The :unique option for `delegate' is deprecated. Use :multiple instead. :unique will be removed in ActiveFedora 7.
    #title
    delegate :title, :to=>"descMetadata"
    delegate :title_nonSort, :to=>"descMetadata", :unique=>"true"
    delegate :title_main_title, :to=>"descMetadata", :unique=>"true"
    delegate :title_sub_title, :to=>"descMetadata", :unique=>"true"

    #Author
    #delegate :author_name, :to=>"descMetadata", :unique=>"true"
    delegate :author_family, :to=>"descMetadata", :unique=>"true"
    delegate :author_given, :to=>"descMetadata", :unique=>"true"
    delegate :a_role, :to=>"descMetadata", :unique=>"true"

    #personal_names
    delegate :pers_first_names, :to=>"descMetadata"
    delegate :pers_last_names, :to=>"descMetadata"
    delegate :pers_roles_text, :to=>"descMetadata"

    #corporate names
    delegate :corporate_names, :to=>"descMetadata"
    delegate :c_role, :to=>"descMetadata", :unique=>"true"

    #Type of Resource
    delegate :res_type, :to=>"descMetadata", :unique=>"true"

    #genre => marcgt
    delegate :g_marcgt, :to=>"descMetadata", :unique=>"true"

    #genre => local
    delegate :g_local, :to=>"descMetadata"

    #origin info
    delegate :place_code, :to=>"descMetadata", :unique=>"true"
    delegate :place_text, :to=>"descMetadata", :unique=>"true"
    delegate :publisher_name, :to=>"descMetadata", :unique=>"true"
    delegate :create_key_date, :to=>"descMetadata", :multiple=>"false"
    delegate :year, :to=>"descMetadata", :multiple=>"false"
    #delegate :year_enc, :to=>"descMetadata"
    delegate :origin_issuance, :to=>"descMetadata", :unique=>"true"

    #Identifier
    delegate :oclc, :to=>"descMetadata", :unique=>"true"
    delegate :isbn, :to=>"descMetadata", :unique=>"true"
    delegate :name, :to=>"descMetadata", :unique=>"true"
    delegate :aleph_sys_number, :to=>"descMetadata", :unique=>"true"
 
    #Language
    delegate :lang_name, :to=>"descMetadata", :unique=>"true"

    #Physical description
    delegate :marcform, :to=>"descMetadata", :unique=>"true"
    delegate :gmd, :to=>"descMetadata", :unique=>"true"
    delegate :marccategory, :to=>"descMetadata", :unique=>"true"
    delegate :marcsmd, :to=>"descMetadata", :unique=>"true"
    delegate :media, :to=>"descMetadata"
    delegate :extent, :to=>"descMetadata", :unique=>"true"
    delegate :digital_origin, :to=>"descMetadata", :unique=>"true"

    #Abstract
    delegate :abstract_para, :to=>"descMetadata"

    #note
    delegate :notes, :to=>"descMetadata"

    #Physical Location
    delegate :phy_loc, :to=>"descMetadata", :unique=>"true"
    delegate :sub_loc, :to=>"descMetadata", :unique=>"true"
    delegate :shelf_loc, :to=>"descMetadata", :unique=>"true"

    #Proquest URL
    delegate :proquest_loc, :to=>"descMetadata", :unique=>"true"

    #ADRL URL
    delegate :adrl_loc, :to=>"descMetadata", :unique=>"true"
    delegate :adrl_preview, :to=>"descMetadata", :unique=>"true"

    #record_info
    delegate :desc_std, :to=>"descMetadata", :unique=>"true"
    delegate :record_content_src, :to=>"descMetadata", :unique=>"true"
    delegate :record_date_create, :to=>"descMetadata", :unique=>"true"
    delegate :record_date_change, :to=>"descMetadata", :unique=>"true"
    #delegate :record_ocolc, :to=>"descMetadata", :unique=>"true"
    #delegate :record_id, :to=>"descMetadata", :unique=>"true"
    delegate :rec_origin, :to=>"descMetadata", :unique=>"true"
    delegate :rec_lang, :to=>"descMetadata", :unique=>"true"

    #subject
    delegate :subjects, :to=>"descMetadata"

    #extension
    delegate :ext_degree_name, :to=>"descMetadata", :unique=>"true"
    delegate :ext_degree_level, :to=>"descMetadata", :unique=>"true"
    delegate :extension, :to=>"descMetadata", :unique=>"true"

end

