class Ability
  include CanCan::Ability
  include Hydra::Ability


    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities

#  def initialize(user)
    # ONLY REGISTERED USERS CAN DO THESE THINGS
#    unless @user.nil?

      # ADMIN
#      if(@user.groups.include?("admin"))
#        can [:create], :all 
#        can [:destroy], ETD
#      end
#    end
#  end

  def user_groups
    return @user_groups if @user_groups
    
    @user_groups = @user.groups + default_user_groups
    @user_groups
  end

#  def custom_permissions#(user, session)
#    if @user.groups.include?("archivist")
#      can :read, ETD
#    end
#  end

  def custom_permissions#(user, session)
    #permissions for public groups
    #if @user.groups.include?("public")
    #if user.role? :public
    #if current_user.public?
    if @user.groups.include? 'public'
      can [:discover], :ETD
      cannot [:read], :ETD
      cannot [:edit], :ETD
      cannot [:delete], ETD
    end

    #permissions for community groups
    #if @user.groups.include?("community")
    if @user.groups.include? 'community'
      can [:discover], :ETD
      can [:read], :ETD
      cannot [:edit], :ETD
      cannot [:delete], ETD
    end

    #permissions for staff groups
    #if @user.groups.include?("staff")
    if @user.groups.include? 'staff'
      can [:discover], :ETD
      can [:read], :ETD
      can [:edit], :ETD
      cannot [:delete], ETD
    end

    #if @user.groups.include?("admin")
    if @user.groups.include? 'admin'
      can [:discover], :ETD
      can [:read], :ETD
      can [:edit], :ETD
      can [:delete], ETD
      #can [:create], :all 
      #can [:destroy], ETD
    end

  end

  # Define any customized permissions here.  Some commented examples are included below.
  #def custom_permissions

    # Limits deleting objects to a the admin user
    #
    # if current_user.admin?
    #   can [:destroy], ActiveFedora::Base
    # end

    # Limits creating new objects to a specific group
    #
    # if user_groups.include? 'special_group'
    #   can [:create], ActiveFedora::Base
    # end

  #end

end
