

class ETDMetadata < ActiveFedora::OmDatastream

  set_terminology do |t|
    t.root(:path=>"mods", :attributes=>{:version=>"3.4"})
    
    t.titleInfo {
      t.nonSort
      t.title
	}
    t.name_per(:path=>"name", :attributes=>{:type=>"personal", :usage=>"primary"}) {
      t.namePart
    }
	t.name_cor(:path=>"name", :attributes=>{:type=>"corporate"}) {
          t.namePart(:path=>"namePart")
	  t.namePart1(:path=>"namePart")
    }
	t.typeOfResource
	t.genre_mar(:path=>"genre", :attributes=>{:authority=>"marcgt"})
	t.genre_loc(:path=>"genre", :attributes=>{:authority=>"local"})
	t.genre_loc1(:path=>"genre", :attributes=>{:authority=>"local"})
	t.originInfo {
	  t.place {
		t.placeTerm(:path=>"placeTerm", :attributes=>{:type=>"code", :authority=>"marccountry"})
	  }
	  t.place {
		t.placeTerm(:path=>"placeTerm", :attributes=>{:type=>"text"})
	  }
	  t.publisher
	  t.dateIssued
	  t.dateIssued_mar(:path=>"dateIssued", :attributes=>{:encoding=>"marc"})
	  t.issuance
	}
	
	t.physicalDescription {
	  t.form_marcfrm(:path=>"form", :attributes=>{:authority=>"marcform"})
	  t.form_gmd(:path=>"form", :attributes=>{:authority=>"gmd"})
	  t.form_auth(:path=>"form", :attributes=>{:authority=>"authority"})
	  t.form_marcsmd(:path=>"form", :attributes=>{:authority=>"marcsmd"})
	  t.extent
	}
	
	t.abstract
	t.note_sor(:path=>"note", :attributes=>{:type=>"statement of responsibility", :altRepGroup=>"00"})
	t.note 
	t.note_thesis(:path=>"note", :attributes=>{:type=>"thesis"})
	t.note_biblio(:path=>"note", :attributes=>{:type=>"bibliography"})
	t.location {
	  t.url(:path=>"url", :attributes=>{:displayLabel=>"electronic resource", :usage=>"primary display", :note=>"ProQuest full text. Restricted to UC campuses"})
	}
	t.identifier(:path=>"identifier", :attributes=>{:type=>"oclc"})
	
	
	t.recordInfo {
	  t.descriptionStandard
      t.recordContentSource(:path=>"recordContentSource", :attributes=>{:authority=>"marcorg"})
      t.recordCreationDate(:path=>"recordCreationDate", :attributes=>{:encoding=>"marc"})
      t.recordChangeDate(:path=>"recordChangeDate", :attributes=>{:encoding=>"iso8601"})
      t.recordIdentifier(:path=>"recordIdentifier", :attributes=>{:source=>"OCoLC"})
      t.recordOrigin
	}
    
  end
  
  def self.xml_template
    Nokogiri::XML.parse('<mods/>')
  end
end
