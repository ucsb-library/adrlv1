

require 'active-fedora'

class SolrService

    def does_index_exists(fieldName, fieldvalue)
     # initialize active fedora with local config
     ActiveFedora.init(:fedora_config_path => 'config/fedora.yml') 
     # query solr for existence of index field with value 
     solr_result = ActiveFedora::SolrService.query(fieldName  + ':' + fieldvalue)
     
     # if record exists and is active then return index exists 
     solr_result.each do|record|
       if record['object_state_ssi'] == 'A'
         return true
       end
     end
 
    false

    end


#    def get_total_object_count()
#     # initialize active fedora with local config
#     ActiveFedora.init(:fedora_config_path => 'config/fedora.yml')
#     solr_result = ActiveFedora::SolrService.count("object_state_ssi:A")
#     return solr_result 
#    end

    def find_matching_encoding_ark(fieldName, fieldvalue)
     # initialize active fedora with local config
     ActiveFedora.init(:fedora_config_path => 'config/fedora.yml')
     # query solr for existence of index field with value 
     solr_result = ActiveFedora::SolrService.query(fieldName  + ':' + fieldvalue)
     
     # if record exists and is active then return index exists 
     solr_result.each do|record|
       if record['object_state_ssi'] == 'A'
         @enc_ark_id = record['id']
	 @pdf = ETD.find(@enc_ark_id).properties.content_file[0]
	 if (@pdf == fieldvalue)
	   @ark_id = @enc_ark_id
	   return @ark_id
	 end
       end
     end
     
    end

    #create a file with hass key as ark_id and value as pdf_name
    def create_encoding_ark_pdf_name()
      # initialize active fedora with local config
      ActiveFedora.init(:fedora_config_path => 'config/fedora.yml')
      # query solr for existence of index field with value
      solr_result = ActiveFedora::SolrService.query("object_state_ssi:A", :rows => 1000)

      # if record exists and is active then return index exists
	  File.open('ark_pdf.csv', 'w') do |f2|
      solr_result.each do|record|
        @enc_ark_id = record['id']
        @pdf = ETD.find(@enc_ark_id).properties.content_file[0]
	#File.open('ark_pdf.csv', 'w') do |f2|
          f2.puts (@enc_ark_id.to_s + ',' + @pdf.to_s)
        #end
      end
        end
    end


end



#MAIN

#s = SolrService.new
#puts s.does_index_exists("content_file_tesim", "Winkler_ucsb_0035_11066.pdf")

s = SolrService.new
#puts s.find_matching_encoding_ark("content_file_tesim", "Day_ucsb_0035D_10992.pdf")
puts s.create_encoding_ark_pdf_name()

#END






