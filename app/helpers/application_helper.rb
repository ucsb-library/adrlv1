module ApplicationHelper

  def date_format_helper args
    DateTime.iso8601( args[:document][args[:field]] ).strftime( "%Y-%m-%d %H:%M:%S" )
  end

  def note_format_helper args
    (args[:document][args[:field]])[1].to_s
    #s = (args[:document][args[:field]])
    #i = s[0].to_s + '<br/>' + s[1].to_s
  end

  def genre_format_helper args
    #(args[:document][args[:field]])[1].to_s
    s = (args[:document][args[:field]])
    s[0].to_s
    #i = s[0].to_s + '<br/>' + s[1].to_s
  end

  def published_format_helper args
    s = (args[:document][args[:field]])[0].split(' ')
    pub = ""
    (1..(s.count-3)).each do |k|
	#if(!s[k].includes?(']'))
	pub.concat(s[k]).concat(' ')
    end
    pub = pub.sub!(']', "] : ")
    #pub = pub.gsub!(/\D/, ",")
  end

  def keywords_format_helper args
    #s = (args[:document][args[:field]]).split(' ')
    s = (args[:document][args[:field]])
    #(0..s.count).each do |k|
	#(args[:document][args[:field]])[0]
    #end
    s.each do |k|
      puts k
      content_tag(:p, k.to_s)
    end
#s[0] + '\n' + s[1]

  end

  def thesis_advisor_helper args
    str_args = %w[]
    str_args.to_a
    thesis_advisor_arr = %w[]
    thesis_advisor_arr.to_a
    str_args = ((args[:document][args[:field]]))
    str_args.each_with_index do |item, index|
      thesis_advisor = "#{item}"
      thesis_advisor = thesis_advisor.sub("\n", "")
      thesis_advisor = thesis_advisor.sub("\n", ",")
      thesis_advisor = thesis_advisor.gsub("\n", "")
      if((thesis_advisor.to_s).include?("Thesis advisor"))
        #puts thesis_advisor 
        thesis_advisor = thesis_advisor.sub("Thesis advisor", "")
        thesis_advisor_arr.push(thesis_advisor)
      end
    end

    thesis_advisor_arr

  end

  def committee_member_helper args
    str_args = %w[]
    str_args.to_a
    comm_member_arr = %w[]
    comm_member_arr.to_a
    str_args = ((args[:document][args[:field]]))
    str_args.each_with_index do |item, index|
      comm_member = "#{item}"
      comm_member = comm_member.sub("\n", "")
      comm_member = comm_member.sub("\n", ",")
      comm_member = comm_member.gsub("\n", "")
      if((comm_member.to_s).include?("Committee member"))
	#puts comm_member
	comm_member = comm_member.sub("Committee member", "")
	comm_member_arr.push(comm_member)
      end
    end	

    comm_member_arr

  end

  def degree_grantor_helper args
    str_args = %w[]
    str_args.to_a
    degree_grantor_arr = %w[]
    degree_grantor_arr.to_a
    str_args = ((args[:document][args[:field]]))
    str_args.each_with_index do |item, index|
      degree_grantor = "#{item}"
      degree_grantor = degree_grantor.sub("\n", "")
      degree_grantor = degree_grantor.sub("\n", ",")
      degree_grantor = degree_grantor.gsub("\n", "")
      degree_grantor_arr.push(degree_grantor.to_s)
    end

    degree_grantor_arr

  end

  def discipline_helper args
    str_args = %w[]
    str_args.to_a
#    str_args = ((args[:document][args[:field]])).split(' ')
    str_args = (args[:document][args[:field]])[0].split(' ')

    discipline = ""
    (2..(str_args.count-1)).each do |k|
        discipline.concat(str_args[k]).concat(' ')
    end
    discipline

  end

#def field_value_separator
#  '<br/>'
#end

  def find_client_ip
    @req_ip = request.remote_ip
    #@rem_ips = @env['HTTP_X_FORWARDED_FOR']
    @remote_ip = request.env["HTTP_X_FORWARDED_FOR"]

    @remote_ip
  end


end
