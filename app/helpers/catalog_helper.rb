require 'date'

module CatalogHelper
  include Blacklight::CatalogHelperBehavior

  def find_client_ip
    @req_ip = request.remote_ip
    #@rem_ips = @env['HTTP_X_FORWARDED_FOR']
    @remote_ip = request.env["HTTP_X_FORWARDED_FOR"]
  end

  def check_on_campus
    on_campus = session[:on_campus]
    if (on_campus.nil?)
      session[:on_campus] = false
      @req_ip = request.remote_ip
      #@rem_ips = @env['HTTP_X_FORWARDED_FOR']
      @remote_ip = request.env["HTTP_X_FORWARDED_FOR"]
      if (@req_ip.start_with?("128.111") || @req_ip.start_with?("169.231"))
        @session_ip = @req_ip
        session[:on_campus] = true
      end
    end
  end

  # this method loads the hashmap of icons path based on the fedora mime-types
  def load_file_icons
    @icons_hash = {}
    CSV.foreach('config/file_icons_mappings.csv') do |row|
      @icons_hash[row[0]] = row[1]
    end
    @icons_hash
  end

  # this method loads the hashmap of extensions based on the fedora mime-types
  def load_file_extensions
    @ext_hash = {}
    CSV.foreach('config/extension_mappings.csv') do |row|
      @ext_hash[row[0]] = row[1]
    end
    @ext_hash
  end

  # format date to display for embargoed etds message
  def format_embargoed_date(embargo_date)
    date = DateTime.parse(embargo_date.to_s)
    formatted_date = date.strftime("%B %d, %Y")
    return formatted_date
  end

end
