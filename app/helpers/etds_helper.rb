module EtdsHelper
  # This method adds the institute id to be included in the display on the pages
  def format_ark(ark_id)
    @config = ConfigReader.new("config/ingest.yml")
    @institute_id = (@config.get_value('institute_id')).to_s
    @institute_id.insert(0, '/')
    @institute_id.insert(6, '/')
    new_ark_id = ark_id
    #new_ark_id.insert(4, @institute_id)
    new_ark_id = new_ark_id.gsub(':', ':' + @institute_id)
    @adrl_url = APP_CONFIG['adrl_url']
    @ark_url = APP_CONFIG['ark_url']
    new_ark_id = new_ark_id.gsub(@adrl_url, @ark_url)
    return new_ark_id 
  end
  #helper_method :format_ark

  def getOriginalARK(ark_id)
    @config = ConfigReader.new("config/ingest.yml")
    @institute_id = (@config.get_value('institute_id')).to_s
    #e1 = ark_id.to(3)
    #e2 = ark_id.from(11)
    #orig_ark_id = e1+e2

    orig_ark_id = ark_id
    orig_ark_id = orig_ark_id.gsub(':/'+@institute_id+'/', ':')
    #orig_ark_id = orig_ark_id.gsub(@ark_url, @adrl_url)
    return orig_ark_id
  end

  def getSuppContentCount(etd)
    count = nil
    begin
      (0..4).each do |c|
        if (!etd.send('suppContent'+c.to_s()).nil?)
          count = c 
        end
      end
    rescue
      return count
    end
    return count
  end

end
