require 'net/http'
#require './app/models/solr_service.rb'
#require '/rails/sc_hydra_head/app/models/solr_service.rb'

class WelcomeController < ApplicationController

  def index()
    #reset_session
    session[:on_campus] = false
    @splash_page=true
    #total_object_count()
    @req_ip = request.remote_ip
    #@rem_ips = @env['HTTP_X_FORWARDED_FOR']
    @remote_ip = request.env["HTTP_X_FORWARDED_FOR"]
    if (@req_ip.start_with?("128.111") || @req_ip.start_with?("169.231"))
	@session_ip = @req_ip
	session[:on_campus] = true
    end
  end

  def splash()
    @splash_page=true
  end

  def content()
    url = APP_CONFIG['fed_url']
    count_ucsb = params[:cu]

    #retrieve from s_table
    @s_hash = session[:s_table]
    @path = nil

    if(!@s_hash.nil? && !@s_hash.blank?)
      s_values = @s_hash[count_ucsb.to_s]

      @session_pid = s_values[0]
      @dsid = s_values[1]
      @name = s_values[2]
      @type = s_values[3]
      @path = url + 'fedora/objects/' + @session_pid + '/datastreams/' + @dsid + '/content'
    end

    if(!@path.nil? && !@path.blank?)
	uri = URI(@path)
	@file_object = Net::HTTP.get(uri)

	#code for send_file
	send_data @file_object, filename: @name, type: @type, disposition: 'inline'
    elsif
	render :file => "welcome/access_error"
    end
    
  end

#  def total_object_count()
#    solr = SolrService.new
#    @count = solr.get_total_object_count()
#  end

end
