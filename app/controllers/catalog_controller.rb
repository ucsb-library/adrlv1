# -*- encoding : utf-8 -*-
require 'blacklight/catalog'
#require './app/models/solr_service.rb'

class CatalogController < ApplicationController

  include Blacklight::Catalog
  include Hydra::Controller::ControllerBehavior
  include BlacklightAdvancedSearch::ParseBasicQ
  include Blacklight::SolrHelper

  #helper_method :find_client_ip

  # These before_filters apply the hydra access controls
  #before_filter :enforce_show_permissions, :only=>[:show]
  # This applies appropriate access controls to all solr queries
  CatalogController.solr_search_params_logic += [:add_access_controls_to_solr_params]
  # This filters out objects that you want to exclude from search results, like FileAssets
  CatalogController.solr_search_params_logic += [:exclude_unwanted_models]


  configure_blacklight do |config|
    config.default_solr_params = {
      :qt => 'search',	
      :rows => 10
      #:q => '*.*' 
      #("hl.fl").to_sym => "title",
      #("hl.simple.pre").to_sym => '<span class="label label-info">',
      #("hl.simple.post").to_sym => "</span>",
      #:hl => true
    }

    config.default_document_solr_params = {
      #("hl.fl").to_sym => "title",
      #("hl.simple.pre").to_sym => '<span class="label label-info">',
      #("hl.simple.post").to_sym => "</span>",
      #:hl => true
    }

    # solr field configuration for search results/index views
    config.index.show_link = 'title_tesim'
    config.index.record_display_type = 'has_model_ssim'

    # solr field configuration for document/show views
    config.show.html_title = 'title_tesim'
    config.show.heading = 'title_tesim'
    config.show.display_type = 'has_model_ssim'

    # solr fields that will be treated as facets by the blacklight application
    #   The ordering of the field names is the order of the display
    #
    # Setting a limit will trigger Blacklight's 'more' facet values link.
    # * If left unset, then all facet values returned by solr will be displayed.
    # * If set to an integer, then "f.somefield.facet.limit" will be added to
    # solr request, with actual solr request being +1 your configured limit --
    # you configure the number of items you actually want _tsimed_ in a page.
    # * If set to 'true', then no additional parameters will be sent to solr,
    # but any 'sniffed' request limit parameters will be used for paging, with
    # paging at requested limit -1. Can sniff from facet.limit or
    # f.specific_field.facet.limit solr request params. This 'true' config
    # can be used if you set limits in :default_solr_params, or as defaults
    # on the solr side in the request handler itself. Request handler defaults
    # sniffing requires solr requests to be made with "echoParams=all", for
    # app code to actually have it echo'd back to see it.
    #
    # :show may be set to false if you don't want the facet to be drawn in the
    # facet bar
    config.add_facet_field solr_name('object_type', :facetable), :label => 'Format'
    config.add_facet_field solr_name('person_full_name_cid_facet', :facetable), :label => 'Names'
#    config.add_facet_field solr_name('origin_info_date_issued', :facetable), :label => 'Publication Year'
#    config.add_facet_field solr_name('subject_topic', :facetable), :label => 'Topic', :limit => 20
#    config.add_facet_field solr_name('origin_info_place_text', :facetable), :label => 'Locations'
#    config.add_facet_field solr_name('title', :facetable), :label => 'Title', :limit => 20

    config.add_facet_field solr_name('person', :facetable), :label => 'Author/Contributor', :limit => 20
    #config.add_facet_field solr_name('person', :facetable), :label => 'Author/Contributor', :limit => 20, :helper_method => :genre_format_helper
    #config.add_facet_field solr_name('person_namePart', :facetable), :label => 'Author_name_split', :limit => 20
    #config.add_facet_field solr_name(:field_name => ('names_family_name','names_given_name'), :facetable, :label => 'Author_name_split', :limit => 20)
    
    #config[:facet] = {
#    config.add_facet_field solr_name{
#	:field_names => (facet_fields = ["names_family_name", "names_given_name"]),:label => "Author_name_split",:limits => 20}

#    config.add_facet_field solr_name('personal_full_name', :facetable), :label => 'Author_full_name', :limit => 20
#    config.add_facet_field solr_name('names', :facetable), :label => 'Personal_names', :limit => 20
#    config.add_facet_field solr_name('personal_names', :facetable), :label => 'Author/Contributor', :limit => 20

    config.add_facet_field solr_name('corporate_names', :facetable), :label => 'Department', :limit => 20
    #config.add_facet_field solr_name('corporate_names', :facetable), :label => 'Department', :limit => 20, :separator => '<br/>'
    #config.add_facet_field solr_name('organization', :facetable), :label => 'Department', :limit => 20
    config.add_facet_field solr_name('genre', :facetable), :label => 'Genre', :limit => 20
    config.add_facet_field solr_name('create_key_date', :facetable), :label => 'Date', :limit => 20
    #config.add_facet_field solr_name('origin_info_date', :facetable), :label => 'Dates', :limit => 20
    config.add_facet_field solr_name('language_lang_code', :facetable), :label => 'Language', :limit => 20
    config.add_facet_field solr_name('ext_degree_level', :facetable), :label => 'Degree Level', :limit => 20
    #config.add_facet_field solr_name('ext_degree_discipline', :facetable), :label => 'Discipline', :limit => true
    config.add_facet_field solr_name('ext_degree_discipline', :facetable), :label => 'Discipline', :limit => 20

    # Have BL send all facet field names to Solr, which has been the default
    # previously. Simply remove these lines if you'd rather use Solr request
    # handler defaults, or have no facets.
    config.default_solr_params[:'facet.field'] = config.facet_fields.keys
    #use this instead if you don't want to query facets marked :show=>false
    #config.default_solr_params[:'facet.field'] = config.facet_fields.select{ |k, v| v[:show] != false}.keys


    # solr fields to be displayed in the index (search results) view
    #   The ordering of the field names is the order of the display
    #config.add_index_field solr_name('ark', :stored_searchable, type: :string), :label => 'ARK:'
    #config.add_index_field solr_name('title', :stored_searchable, type: :string), :label => 'Title:'
    # commented for highlight changes
    #config.add_index_field solr_name('title', :stored_searchable, type: :string), :label => 'Title:', :highlight => true
    config.add_index_field solr_name('author_part', :stored_searchable, type: :string), :label => 'Author:'

    config.add_index_field solr_name('origin_info', :stored_searchable, type: :string), :label => 'Published:', :helper_method => :published_format_helper
    #config.add_index_field solr_name('origin_info_publisher', :stored_searchable, type: :string), :label => 'Published:'
    #config.add_index_field solr_name('origin_info_place_text', :stored_searchable, type: :string), solr_name('origin_info_publisher', :stored_searchable, type: :string), :label => 'Published:'
    #config.add_index_field solr_name('origin_info_place_text' + ':' + 'origin_info_publisher', :stored_searchable, type: :string), :label => 'Published:'
    #config.add_index_field solr_name('origin_info_publisher'+':', :stored_searchable, type: :string), :label => 'Published:'

    #config.add_index_field solr_name('origin_info_date_issued_enc', :stored_searchable, type: :string), :label => 'Year:' 
    #config.add_index_field solr_name('date_created_key', :stored_searchable, type: :string), :label => 'Date Created:' 
    config.add_index_field solr_name('create_key_date', :stored_searchable, type: :string), :label => 'Date Created:' 
    #config.add_index_field solr_name('isbn', :stored_searchable, type: :string), :label => 'ISBN:'
    config.add_index_field solr_name('object_type', :stored_searchable, type: :string), :label => 'Format:'


    # Highlight
    #config.add_index_field 'ark', :highlight => true
    #config.add_facet_field solr_name('object_type', :facetable), :label => 'Format', :show => false
    #config.add_field_configuration_to_solr_request!

    # solr fields to be displayed in the show (single result) view
    #   The ordering of the field names is the order of the display
    #config.add_show_field solr_name('title', :stored_searchable, type: :string), :label => 'Title:'
    # commented for highlight changes
    #config.add_show_field solr_name('title', :stored_searchable, type: :string), :label => 'Title:', :highlight => true
    config.add_show_field solr_name('author_part', :stored_searchable, type: :string), :label => 'Author:'
    config.add_show_field solr_name('origin_info', :stored_searchable, type: :string), :label => 'Published:', :helper_method => :published_format_helper
    #config.add_show_field solr_name('origin_info_date_issued_enc', :stored_searchable, type: :string), :label => 'Year:'
    config.add_show_field solr_name('create_key_date', :stored_searchable, type: :string), :label => 'Date Created:'
    config.add_show_field solr_name('object_type', :stored_searchable, type: :string), :label => 'Format:'
    config.add_show_field solr_name('extent', :stored_searchable, type: :string), :label => 'Description:'

    #config.add_show_field solr_name('note_advisor', :stored_searchable, type: :string), :label => 'Notes:'
    config.add_show_field solr_name('note_advisor', :stored_searchable, type: :string), :label => 'Notes:', :helper_method => :note_format_helper

    config.add_show_field solr_name('note_thesis', :stored_searchable, type: :string), :label => 'Dissertation:'
    config.add_show_field solr_name('note_bibliography', :stored_searchable, type: :string), :label => 'Bibliography:'
    config.add_show_field solr_name('abstract', :stored_searchable, type: :string), :label => 'Summary:'
    config.add_show_field solr_name('note_sor', :stored_searchable, type: :string), :label => 'Statement of responsibility:'
    
    genre = solr_name('genre_marcgt', :stored_searchable, type: :string)
#    puts "genre"
#    puts genre
    genre_local = solr_name('genre_local', :stored_searchable, type: :string)
#    puts "genre_local"
#    puts genre_local 
    #config.add_show_field (genre + "\n" + genre_local), :label => 'Genre:'
    #config.add_show_field (solr_name('genre_marcgt', :stored_searchable, type: :string)+":"+solr_name('genre_local', :stored_searchable, type: :string)), :label => 'Genre:'

    config.add_show_field genre, :label => 'Genre:'
    #config.add_show_field genre_local, :label => '', :separator => '<br/>' 
    config.add_show_field genre_local, :label => '', :separator => ' : '

    #config.add_show_field solr_name('subject_topic', :stored_searchable, type: :string), :label => 'Keywords:'
    #config.add_show_field solr_name('subject_topic', :stored_searchable, type: :string), :label => 'Keywords:', :separator => '<br/>', :helper_method => :keywords_format_helper
    config.add_show_field solr_name('subject_topic', :stored_searchable, type: :string), :label => 'Keywords:', :separator => ' : ', :helper_method => :keywords_format_helper
    #config.add_show_field solr_name('subject_topic', :stored_searchable, type: :string), :label => 'Keywords:', :helper_method => :field_value_separator

    #config.add_show_field solr_name('personal_author', :stored_searchable, type: :string), :label => 'Thesis advisor:', :helper_method => :thesis_advisor_helper, :separator => '<br/>'
    config.add_show_field solr_name('personal_author', :stored_searchable, type: :string), :label => 'Thesis advisor:', :helper_method => :thesis_advisor_helper, :separator => ' : '
    #config.add_show_field solr_name('advisor', :stored_searchable, type: :string), :label => 'advisor:' 
    #config.add_show_field solr_name('committee_member', :stored_searchable, type: :string), :label => 'Committee member:', :helper_method => :committee_member_helper, :separator => '\n'
    config.add_show_field solr_name('committee_member', :stored_searchable, type: :string), :label => 'Committee member:', :helper_method => :committee_member_helper, :separator => ' : '
    #config.add_show_field solr_name('name', :stored_searchable, type: :string), :label => 'Committee member:', :helper_method => :committee_member_helper("Committee member"), :separator => '<br/>'
    config.add_show_field solr_name('corporate_names', :stored_searchable, type: :string), :label => 'Degree grantor:', :helper_method => :degree_grantor_helper, :separator => '. '

    #config.add_show_field solr_name('extension', :stored_searchable, type: :string), :label => 'Discipline:', :helper_method => :discipline_helper, :separator => '\n'
    #config.add_show_field solr_name('extension', :stored_searchable, type: :string), :label => 'Discipline:', :helper_method => :discipline_helper, :separator => '<br/>'
    #config.add_show_field solr_name('ext_degree_discipline', :stored_searchable, type: :string), :label => 'Discipline:', :separator => '<br/>'
    config.add_show_field solr_name('ext_degree_discipline', :stored_searchable, type: :string), :label => 'Discipline:', :separator => ' : '

    config.add_show_field solr_name('extension_discipline', :stored_searchable, type: :string), :label => 'Discipline:'
    config.add_show_field solr_name('language_lang_code', :stored_searchable, type: :string), :label => 'Language:'

    config.add_show_field solr_name('isbn', :stored_searchable, type: :string), :label => 'ISBN:'
    config.add_show_field solr_name('ark', :stored_searchable, type: :string), :label => 'ARK:'
    config.add_show_field solr_name('adrl_location_url', :stored_searchable, type: :string), :label => 'Permalink:'
    config.add_show_field solr_name('copyright_description', :stored_searchable, type: :string), :label => 'Copyright:'
    #config.add_show_field :string => 'UC Santa Barbara only', :label => 'Access:'

    #config.add_show_field solr_name('system_create', :stored_sortable, type: :date), :label => 'Creation time:', :helper_method => :date_format_helper
    #config.add_show_field solr_name('system_create', :stored_sortable, type: :date), :label => 'Creation time:'


    # "fielded" search configuration. Used by pulldown among other places.
    # For supported keys in hash, see rdoc for Blacklight::SearchFields
    #
    # Search fields will inherit the :qt solr request handler from
    # config[:default_solr_parameters], OR can specify a different one
    # with a :qt key/value. Below examples inherit, except for subject
    # that specifies the same :qt as default for our own internal
    # testing purposes.
    #
    # The :key is what will be used to identify this BL search field internally,
    # as well as in URLs -- so changing it after deployment may break bookmarked
    # urls.  A display label will be automatically calculated from the :key,
    # or can be specified manually to be different.

    # This one uses all the defaults set by the solr request handler. Which
    # solr request handler? The one set in config[:default_solr_parameters][:qt],
    # since we aren't specifying it otherwise.

    config.add_search_field 'all_fields', :label => 'All Fields'


    # Now we see how to over-ride Solr request handler defaults, in this
    # case for a BL "search field", which is really a dismax aggregate
    # of Solr search fields.

    config.add_search_field('title') do |field|


      #NEW
      #field.solr_parameters = { :'spellcheck.dictionary' => 'title' }
 
      # :solr_local_parameters will be sent using Solr LocalParams
      # syntax, as eg {! qf=$title_qf }. This is neccesary to use
      # Solr parameter de-referencing like $title_qf.
      # See: http://wiki.apache.org/solr/LocalParams
      field.qt = 'search'
      field.solr_local_parameters = {
        :qf => '$title_qf',
        :pf => '$title_pf'
      }
    end

    config.add_search_field('author') do |field|
      field.solr_local_parameters = {
        #:qf => '$author_qf',
        #:pf => '$author_pf'
        :qf => '$person_qf',
        :pf => '$person_pf'
      }
    end

    config.add_search_field('Subject') do |field|
      field.solr_local_parameters = {
        :qf => '$genre_qf',
        :pf => '$genre_pf'
      }
    end

    # Specifying a :qt only to show it's possible, and so our internal automated
    # tests can test it. In this case it's the same as
    # config[:default_solr_parameters][:qt], so isn't actually neccesary.
    config.add_search_field('Identifier') do |field|
      field.qt = 'search'
      field.solr_local_parameters = {
        :qf => '$identifier_qf',
        :pf => '$identifier_pf'
      }
    end

#    config.add_search_field('PDF Name') do |field|
#      field.qt = 'search'
#      field.solr_local_parameters = {
#        :qf => '$content_file_qf',
#        :pf => '$content_file_pf'
#      }
#    end
#
#    config.add_search_field('Committee Member') do |field|
#      field.qt = 'search'
#      field.solr_local_parameters = {
#        :qf => '$person_qf',
#        :pf => '$person_pf'
#      }
#    end
#
#    config.add_search_field('Thesis Advisor') do |field|
#      field.qt = 'search'
#      field.solr_local_parameters = {
#        :qf => '$person_qf',
#        :pf => '$person_pf'
#      }
#    end
#

    # "sort results by" select (pulldown)
    # label in pulldown is followed by the name of the SOLR field to sort by and
    # whether the sort is ascending or descending (it must be asc or desc
    # except in the relevancy case).
    config.add_sort_field 'score desc, pub_date_dtsi desc, title_tesi asc', :label => 'relevance'
    config.add_sort_field 'pub_date_dtsi desc, title_tesi asc', :label => 'year'
    config.add_sort_field 'author_tesi asc, title_tesi asc', :label => 'author'
    config.add_sort_field 'title_tesi asc, pub_date_dtsi desc', :label => 'title'
    #config.add_sort_field 'title asc', :label => 'title'

    # If there are more than this many search results, no spelling ("did you
    # mean") suggestion is offered.
    config.spell_max = 5

  end

  def exclude_unwanted_models(solr_parameters, user_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << "-object_state_ssi:D"

    #total objects count
    #total_object_count()

  end

#  def find_client_ip
#    @req_ip = request.remote_ip
#    #@rem_ips = @env['HTTP_X_FORWARDED_FOR']
#    @remote_ip = request.env["HTTP_X_FORWARDED_FOR"]
#  end

#  def find_total_count()
#    count = @response.total_count
#    count
#  end

#  def total_object_count()
#    solr = SolrService.new
#    @count = solr.get_total_object_count()
#  end

  #def ucsb_email
#  def email
    #UcsbMailer.ucsb_email1(params[:to],params[:frome],params[:messagee]).deliver
    #UcsbMailer.email(params[:to],params[:message]).deliver
    #Catalog.email_sent(params[:to],params[:message]).deliver

#    @documents = current_user.documents
#    RecordMailer.email_record(@documents, {:to => params[:to], :message => params[:message]}, url_options).deliver
    #render :text => "Email sent successfully"
#  end

  def email
  #  @response, @documents = get_solr_response_for_document_ids(params[:id])
     @response, @documents = get_solr_response_for_field_values(SolrDocument.unique_key,params[:id])

     if request.post? and validate_email_params
       email = RecordMailer.email_record(@documents, {:to => params[:to], :message => params[:message]}, url_options)
       email.deliver 

       flash[:success] = I18n.t("blacklight.email.success")

       respond_to do |format|
        #format.html { redirect_to catalog_path(params['id']) }
        format.html { redirect_to "/bookmarks" }
        format.js { render 'email_sent' }
       end and return
     end

     respond_to do |format|
      format.html
      format.js { render :layout => false }
     end
  end


#  def ucsb_form
#    UcsbMailer.ucsb_email1("toa@sis-inc.net",params[:email],params[:message]).deliver
#    render :text => "File has been ingested"
#  end

end
