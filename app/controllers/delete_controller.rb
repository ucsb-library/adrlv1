require 'active-fedora'
#require '../models/datastreams/suppress_metadata.rb'
#require '../models/suppress.rb'

class DeleteController < ApplicationController

#  def initialize()
#  end

  # show method is called when the ETD is deleted and its displays the information on delete_confirm.html.erb page.
  def show 
    #show
    pid = params[:id]
    @etd = ETD.find(pid)
  end

  # delete_confirm submits ETD to be suprressed when the form is submitted from show.html.erb page.
  def delete_confirm
    pid = params[:pid]
    reason = params[:reason_delete]
    @etd = ETD.find(pid)
    @etd.inner_object.state = 'D'

    # adds a new property to the existing properties datastream which contains the reason for suppression.
    @etd.properties.suppress_reason = reason

    @etd.save
    # @etd.update_index
    #render :file => 'etds/delete_confirm.html.erb'
  end
end
