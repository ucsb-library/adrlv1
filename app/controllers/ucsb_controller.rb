class UcsbController < ApplicationController

  def ucsb_email
    UcsbMailer.ucsb_email1(params[:toe],params[:frome],params[:messagee]).deliver
    render :text => "File has been ingested"
  end

# The Action below handles the Contact Us Form in the ADRL. It uses an invisible
# field for the Zip Code as a Honeypot for Spam Bots. If that field is filled in
# then it is currently labeled as [ADRL SPAMBOT]. Once this is fully tested and
# found effective we can just redirect such spam to a different e-mail address.

  def ucsb_form
    #UcsbMailer.ucsb_email1("toa@sis-inc.net",params[:email],params[:message]).deliver
    @to_email = APP_CONFIG['to_email']
    subjheader = "[ADRL SPAMBOT?] "
    subjheader = "[ADRL] " if params[:zipcode].nil? || params[:zipcode].empty?
    #UcsbMailer.ucsb_email1(params[:email],params[:message]).deliver
    #UcsbMailer.ucsb_email1(@to_email, params[:message]).deliver

    UcsbMailer.ucsb_contactus_email("ADRL <" + @to_email + ">", \
                                    params[:name] + " <" + params[:email] + ">", \
                                    subjheader + params[:contact_form][:category], \
                                    params[:message]).deliver

# Notify the user who submitted the Contact Us form that e-mail has been sent
# to the ADRL.

    flash[:notice] = "Thank You for the Feedback. Your Submission has been successfully sent to the ADRL."

# The code below redirects back to URL prior to accessing the Contact Us form IF that 
# was within the ADRL. If from outside then the redirect goes to the ADRL home page.

    referURL = params[:referer]
    base_url = APP_CONFIG['base_url']
    if referURL.include? base_url
      redirect_to(params[:referer])
    else
#    redirect_to(:back)
    #render :text => "Email has been sent successfully."
    #render :partial => "ucsb/ucsb_form"
     redirect_to "/"
    end
  end
end
