require 'active-fedora'
#require 'app/models/datastreams/etd_metadata.rb'
#require 'app/models/etd.rb'
#require '../models/config_reader.rb'

class IngestController < ApplicationController

  before_action :show, only: [:show, :edit]
  before_action :ingestFile, only: [:show, :edit, :update]

  #This is commented to enable the search feature on the ingest pages.
  #def initialize()
  #end

  def ingestFile
    #create an array of validation errors 
    @form_errors = %w[]
    @form_errors.to_a()

    name = params[:ingest]
    etd_file_location = params[:etd_location]
    if name.nil?
      @form_errors.push("MARC file can't be blank")
      puts @form_errors[0]
    end
    if etd_file_location.nil? || etd_file_location.blank?
      @form_errors.push("ETD file location can't be blank")
      puts @form_errors[1]
    end

    if !(@form_errors.empty?)
      #redirect_to :back
      render "ingest"
    else
      fileName = params[:ingest].original_filename
      directory = "public/data/marc/"
      # create the file path
      path = File.join(directory, fileName)
      # write the file
      File.open(path, "wb") { |f|
        f.write(name.read) }

      main = Main.new
      post = main.batch_injest(path, etd_file_location)
      @hist = main.get_ingest_log()
      @dup_rec= main.get_duplicate_recs()
      @skip_rec= main.get_skipped_recs()
      puts "controller: hist"
      puts @hist 
      puts @dup_rec 
      puts @skip_rec
      puts "controller: pids"
      puts post 

      #commented list(post) method since the records are read from history logs
      #list(post)

      #render :text => "File has been ingested"
      #render :text => path
      #render :text => etd_file_location
    end
  end

  def list(post)
    # @etds = ETD.find(:all)
    # @etds = ETD.find(:all, :conditions => ["pid in (?)", @post])
    #  @etds = ETD.find(post)

#     solr = SolrService.new
#     (0..post.count).each do |c|
#       if solr.does_index_exists("id", post[c]) 
#	 puts 
#       end
#     end	

    @success_pids = %w[]
    @success_pids.to_a()

    @failed_pids = %w[]
    @failed_pids.to_a()

    post.each do |p| 
      begin
        if(ETD.find(p).nil?)
	    @failed_pids.push(p)
        else
	    @success_pids.push(p)
        end
      rescue
        @failed_pids.push(p)
      end
    end
    puts "success_pids"
    puts @success_pids
    puts "failed_pids"
    puts @failed_pids
    @etds = ETD.find(@success_pids)
  end
  
  def get_institute_id
    c = new ConfigReader("ingest.yml")
    @institute_id = c.getValue("institute_id")
  end

  def show
    @etd = ETD.find(params[:id]) 
  end

end
