require 'active-fedora'
require 'date'
#require '../ucsb/config_reader.rb'

class EtdsController < ApplicationController

  before_filter :check_for_cancel, :only => [:show, :edit]
  before_action :show, only: [:show, :edit]
  before_action :edit, only: [:show, :edit, :update]

  def check_for_cancel
    if(params['cancel'])
	url = APP_CONFIG['rails_url']
        redirect_to (url + 'etds/' + params[:pid])
    end
  end

#  def initialize()
#  end

  def find_client_ip()
    @req_ip = request.remote_ip
    #@rem_ips = @env['HTTP_X_FORWARDED_FOR']
    @remote_ip = request.env["HTTP_X_FORWARDED_FOR"]
  end

  # edit method is called on clicking Edit from show.html.erb page. This retrieves all the params and displays them.
  def edit
    ark_pid = params[:ark_pid]
    if ark_pid.nil? || ark_pid.blank?
	pid = params[:pid]
	if(pid.nil? || pid.blank?)
	  id = params[:id]
	  @etd = ETD.find(id)
	else
	  @etd = ETD.find(pid)
	end
	puts @etd
    else
	puts ark_pid
	@etd = ETD.find(ark_pid)
	puts @etd
    end
    #find_client_ip()
  end

  # show method is called upon submitting the form from edit.html.erb page. This takes all the params and performs update. 
  def show
    #title = params[:title]
    title_main_title = params[:title_main_title]
    puts "if title"
    if title_main_title.nil?
	#show
	pid = params[:id]
	if(pid.nil? || pid.blank?)
	  ark_pid = params[:ark_pid]
	  if (ark_pid.nil? || ark_pid.blank?)
	    pid = params[:pid]
	    @etd = ETD.find(pid)
	  else
	    @etd = ETD.find(ark_pid)
	  end
	else
	  @etd = ETD.find(pid)
	end
    else
    	@pid = params[:pid]
    	@etd = ETD.find(@pid)

	#Update title_info
	title_nonSort = params[:title_nonSort]
	update("title_nonSort", title_nonSort)

	update("title_main_title", title_main_title)

	title_sub_title = params[:title_sub_title]
	update("title_sub_title", title_sub_title)

	language = params[:language]
    	puts @etd.update_attributes({"lang_name" => language})

	#Update Abstract 
        @abstracts= %w[]
        @abstracts.to_a()
	@abstracts= getParams(@etd.abstract_para.count, 'abstract')
	puts @etd.update_attributes({"abstract_para" => @abstracts})

	#Update Author
	author_family = params[:author_family]
	author_given = params[:author_given]
	a_role = params[:a_role]

	#Update personal last names
        @family= %w[]
        @family.to_a()
	# @family = getParams(@etd.pers_last_names.count, 'p_last_name')
	@family = getPersonalParams(@etd.pers_last_names.count, 'p_last_name')
	puts @etd.update_attributes({"pers_last_names" => @family})

	#Update personal first names
        @given= %w[]
        @given.to_a()
	# @given = getParams(@etd.pers_first_names.count, 'p_first_name')
	@given = getPersonalParams(@etd.pers_first_names.count, 'p_first_name')
	puts @etd.update_attributes({"pers_first_names" => @given})

	#Update personal role text 
        @role_text= %w[]
        @role_text.to_a()
	# @role_text = getParams(@etd.pers_roles_text.count, 'p_role_text')
	@role_text = getPersonalParams(@etd.pers_roles_text.count, 'p_role_text')
	puts @etd.update_attributes({"pers_roles_text" => @role_text})

	#Update author	
	puts @etd.update_attributes({"a_role" => a_role})
	puts @etd.update_attributes({"author_family" => author_family, "author_given" => author_given})

	#corporate namec
        @c_names= %w[]
        @c_names.to_a()
	@c_names = getParams(@etd.corporate_names.count, 'c_name')
	puts @etd.update_attributes({"corporate_names" => @c_names})
	c_role = params[:c_role]
	puts @etd.update_attributes({"c_role" => c_role})

	#Type of Resource
	res_type = params[:res_type]
	update("res_type", res_type)

	#Genre => marcgt
	g_marcgt = params[:g_marcgt]
	update("g_marcgt", g_marcgt)

	#Genre => local
        @g_locals= %w[]
        @g_locals.to_a()
	@g_locals= getParams(@etd.g_local.count, 'g_local')
	puts @etd.update_attributes({"g_local" => @g_locals})

	#origin_info	
	place_code = params[:place_code]
	place_text = params[:place_text]
	publisher = params[:publisher]

	#origin_info date created : key_date
	date_created = params[:date_created]
	puts @etd.update_attributes({"create_key_date" => date_created})

        @year = %w[]
        @year.to_a()
	@year = getParams(@etd.year.count, 'year')
	issuance = params[:issuance]
    	puts @etd.update_attributes({"place_code" => place_code, "place_text" => place_text, "publisher_name" => publisher, "year" => @year, "origin_issuance" => issuance})

	#Physical Description
	marcform = params[:marcform]
	update("marcform", marcform)

	gmd = params[:gmd]
	update("gmd", gmd)

	marccategory = params[:marccategory]
	update("marccategory", marccategory)

	marcsmd = params[:marcsmd]
	update("marcsmd", marcsmd)

	#media = params[:media]
	#update("media", media)

	#Update Media 
#        @media_types= %w[]
#        @media_types.to_a()
#	 @media_types= getParams(@etd.media.count, 'media')
#	 puts @etd.update_attributes({"media" => @media_types})

	extent = params[:extent]
	update("extent", extent)

	digital_origin = params[:digital_origin]
	update("digital_origin", digital_origin)

	#puts @etd.update_attributes({"marcform" => marcform, "gmd" => gmd, "marccategory" => marccategory, "marcsmd" => marcsmd, "media" => media, "extent" => extent, "digital_origin" => digital_origin})

	#Physical Location
	physical_loc = params[:physical_loc]
	sub_loc = params[:sub_loc]
	shelf_loc = params[:shelf_loc]
	puts @etd.update_attributes({"phy_loc" => physical_loc, "sub_loc" => sub_loc, "shelf_loc" => shelf_loc})

	#Proquest URL
	proquest_loc = params[:proquest_loc]
	update("proquest_loc", proquest_loc)

	#Record Info
	desc_std = params[:desc_std]
	puts @etd.update_attributes({"desc_std" => desc_std})

 	#update subject   	
        @topics = %w[]
        @topics.to_a()
	@topics = getParams(@etd.subjects.count, 'topic')
	puts @etd.update_attributes({"subjects" => @topics})

 	#update identifier 
	isbn = params[:isbn]
	update("isbn", isbn)

	oclc = params[:oclc]
	update("oclc", oclc)

	aleph = params[:aleph]
	update("aleph_sys_number", aleph)

        @notes= %w[]
        @notes.to_a()
	@notes = getParams(@etd.notes.count, 'note')
	puts @etd.update_attributes({"notes" => @notes})
	
	rec_change_date = DateTime.now
	puts @etd.update_attributes({"record_date_change" => rec_change_date})

    	# @etd.save
    	# @etd.update_index
    end
    #find_client_ip()
  end

  def update(key, value)
    if(value != nil)
      puts @etd.update_attributes({key => value})
    end 
  end

  def getParams(count, param_name)
    @params_a = %w[]
    @params_a.to_a()
    (0..count-1).each do |s|
        @name = param_name + s.to_s()
        puts "name: " + @name
        param_val = params[(@name).to_sym]
        puts param_name+ ": " + param_val.to_s()
        @params_a.push(param_val)
    end
    return @params_a
  end

  def getPersonalParams(count, param_name)
    @params_a = %w[]
    @params_a.to_a()
    (1..count-1).each do |s|
        @name = param_name + s.to_s()
        puts "name: " + @name
        param_val = params[(@name).to_sym]
        puts param_name+ ": " + param_val.to_s()
        @params_a.push(param_val)
    end
    return @params_a
  end

end
