class UcsbMailer < ActionMailer::Base

  @from_email = APP_CONFIG['from_email']
  default from: @from_email

  #def ucsb_email1(toe, frome, messagee)
  #def email(toe, messagee)
  def ucsb_email1(toe, messagee)
    toe = APP_CONFIG['to_email']
    mail(to: toe, body: messagee, content_type: "text/html", subject: "Welcome to ADRL!")
  end

  def ucsb_contactus_email(cto, cfrom, csubj, cmessage)
    mail(to: cto, from: cfrom, subject: csubj, body: cmessage, content_type: "text/html")
  end

end

