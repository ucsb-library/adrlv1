#!/usr/local/rvm/rubies/ruby-2.0.0-p247/bin/ruby
require 'rails/all'

require 'yaml'


class ConfigReader

     @@default_path = 'ingest.yml.prod'

     # load the config file
     def initialize path
       
       if path.nil?
          path = @@default_path
       end

       #TODO: replace rails_env with Rails_env later
       #rails_env = 'test'
       #rails_env = 'production'
       #rails_env = Rails_env
       rails_env = Rails.env

      
       @config = YAML::load_file(path)[rails_env]
     end

     # get the value of single key with prefix
     def get_value(key)
       @config[key] 
     end

     # return hashmap of all properties
     def get_all()
      @config   
     end


end


#MAIN

c = ConfigReader.new(nil)
puts c.get_value('institute_id')


#END




