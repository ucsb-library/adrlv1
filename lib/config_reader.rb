# #!/usr/local/rvm/rubies/ruby-2.0.0-p247/bin/ruby

require 'rails/all'

require 'yaml'


class ConfigReader

     @@default_path = 'ingest.yml'

     # load the config file
     def initialize path
       
       if path.nil?
          path = @@default_path
       end

       #TODO: replace rails_env with Rails_env later
       env = APP_CONFIG['ucsb_env']
       #rails_env = env 
      
       #@config = YAML::load_file(path)[rails_env]
       @config = YAML::load_file(path)[env]

       raise "No settings for environment #{env}" if @config.nil?
       @config

     end

     # get the value of single key with prefix
     def get_value(key)
       @config[key] 
     end

     # return hashmap of all properties
     def get_all()
      @config   
     end


end


#MAIN

#c = ConfigReader.new(nil)
#puts c.get_value('ezid_user')

#c = ConfigReader.new('ingest.yml.prod')
#puts c.get_value('institute_id')

#END




