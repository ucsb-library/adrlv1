#!/usr/local/rvm/rubies/ruby-2.0.0-p247/bin/ruby

require 'active-fedora'
require './app/models/datastreams/mods_article_datastream.rb'
require './app/models/etd.rb'


#MAIN

puts "Do you want to delete all the objects permanently from Fedora repo? [Y/N]"
val = gets.chomp 

if val == 'y' || val == 'Y'

   puts "Deleting all records...."

   ActiveFedora.init(:fedora_config_path => "config/fedora.yml")
   
   # for deleting all objects
   #all_etd = ActiveFedora::Base.find(:all)
 
   # for deleting ETD type objects
   all_etd = ETD.find(:all)
   
   all_etd.each do |etd|
      etd.delete 
   end
  
   puts "Complete"

end

#END
