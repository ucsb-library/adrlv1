require 'active-fedora'
require './app/models/datastreams/suppress_metadata.rb'
require './app/models/datastreams/mods_article_datastream.rb'
require './app/models/etd.rb'

class TEST
def value()
a = %w[]
a.to_a()
a.push({"Ark"=>"ark:/99999/fk4gx6gsn", "Aleph"=>"003978828", "Author"=>"Quady, Maura Colleen", "Title"=>"Improving the hydro-stratigraphic model of the Oxnard Forebay, Ventura County, California, using transient electromagnetic surveying", "Date"=>"2014-01-27T17:45:32-08:00"})
a.push({"Ark"=>"ark:/99999/fk4c55r3s", "Aleph"=>"003978855", "Author"=>"Zdanowicz, Francois.", "Title"=>"Nation-state or state-nationmyths of nationhood and support for state sovereignty in Belarus", "Date"=>"2014-01-27T17:45:36-08:00"})
(0..a.count).each do |h|
  puts a[h].to_s()
  puts a[h].values_at("Ark").to_s()
end
end
end

