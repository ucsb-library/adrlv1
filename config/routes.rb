  #ETD::Application.routes.draw do
  #  resources :etds
  #end

ScHydraHead::Application.routes.draw do
  #root :to => "catalog#index"
  #root :to => "upload#upload"
  #root :to => "upload#uploadfile"
  #root :to => "upload#index"
  Blacklight.add_routes(self)
  HydraHead.add_routes(self)
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'welcome#index'
   #root 'welcome#splash'

  get "welcome/about"
  get "welcome/contact_us"
  get "welcome/off_campus_login"
  #get "welcome/etds_collections"
  #get "welcome/etds_collections"
  #get "catalog/etds_collections"
  #get "welcome/splash"
  #get "welcome/header_splash"
  #get "welcome/new_header_splash"
  get "welcome/login_link"
  get "welcome/content"
  post "welcome/content"

  #ucsb_mailer
  get "catalog/email"
  post "catalog/email1"

  get "ingest/ingest"
  post "ingest/ingestFile"
  #get "ingest/ingestFile" ingest#list
  get "etds/show"
  
  #get "etds/delete"
  #post "etds/delete_confirm"

  #get "etds/edit_detail"
  #post "etds/edit"

  #get "etds/edit"
  #post "etds/edit_detail"

  # Routes for Edit links
  resources :etds
  post "etds/edit"
  post "etds/show"
  
  # Routes for Delete links
  resources :delete
  post "delete/delete_confirm"

  get "search/search"

  get "ucsb/ucsb_form1"
  post "ucsb/ucsb_form"
  get "ucsb/ucsb_emailf"
  post "ucsb/ucsb_email"

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

